<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grouplang extends Model
{
    protected $table = 'group_translate';
    protected $primaryKey = 'id';
}
