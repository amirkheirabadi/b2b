<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    protected $primaryKey = 'id';

    public function orders()
    {
      return $this->hasMany('App\Order','product_id');
    }

    public function company()
    {
      return $this->belongsTo('App\Company','company_id');
    }
}
