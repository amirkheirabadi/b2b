<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
 */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
 */

Route::group(['middleware' => ['web']], function () {

    Route::controller('/auth', 'AuthController');

    Route::group(['middleware' => ['auth'], 'prefix' => 'dashboard'], function () {
        // Customer

        // Company
        // Route::controller('/products', 'ProController');
        // Route::controller('/messages', 'MessageController');
        // Route::controller('/orders', 'Customer\OrderController');

        // admin
        Route::controller('/companies', 'Admin\CompanyController');
        Route::controller('/products', 'Admin\ProController');
        Route::controller('/transactions', 'Admin\TransactionsController');
        Route::controller('/admins', 'Admin\AdminController');
        Route::controller('/users', 'Admin\UserController');
        Route::controller('/orders', 'Admin\OrderController');
        Route::controller('/messages', 'Admin\MessageController');
          Route::controller('/', 'DashboardController');

    });

    Route::controller('/', 'HomeController');
});
