<?php

namespace App\Http\Middleware;

use Closure;
use App\Helper\B2b;
use Illuminate\Support\Facades\Auth;
use App\Country;
use Session;

class langcheck
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $login = false;
        foreach (['company', 'user', 'admin'] as $gu) {
            if (Auth::guard($gu)->check()) {
                $login = true;
                $guard = $gu;
            }
        }

        if (!$login || Auth::guard($gu)->user()['lang_status'] == 'auto') {
            if (Session::has('lang')) {
                App::setlocale(Session::get('lang'));
            } else {
                $countryCode = B2b::location()['countryCode'];
                $country = Country::where('countryCode', $countryCode)->get();

                $languages = Languages::all();
                foreach ($languages as $lang) {
                    if (strpos($country->languages, $lang->code)) {
                        $language = $lang;
                        break;
                    }
                }
                App::setLocale($language->code);
            }
        } else {
            $user = Auth::guard($gu)->user();
            $language = Language::where('id', $user->lang_id)->get();
            App::setlocale($language[0]['code']);
        }

        return $next($request);
    }
}
