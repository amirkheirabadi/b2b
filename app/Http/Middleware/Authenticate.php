<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Authenticate {
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @param  string|null  $guard
	 * @return mixed
	 */
	public function handle(Request $request, Closure $next, $guard = null) {
		if ($guard) {
			if (Auth::guard($guard)->guest()) {
				if ($request->ajax()) {
					return response('Unauthorized.', 401);
				} else {
					return redirect('/auth');
				}
			}
		} else {
			$login = false;
			foreach (['company', 'user', 'admin'] as $gu) {
				if (Auth::guard($gu)->check()) {
					$login = true;
					$guard = $gu;
				}
			}

			if (!$login) {
				if ($request->ajax()) {
					return response('Unauthorized.', 401);
				} else {
					return redirect('/auth');
				}
			}
		}

		$request[$guard] = Auth::guard($guard)->user();
		$request['guard'] = $guard;

		return $next($request);
	}
}
