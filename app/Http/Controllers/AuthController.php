<?php

namespace App\Http\Controllers;

use App\Company;
use App\Helper\B2b;
use App\User;
use Auth;
use Flash;
use Illuminate\Http\Request;
use App\Scope;
use App\Group;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getSignout']);
    }

    public function getIndex(Request $request)
    {
        return view('auth/signin', [
            'title' => trans('auth.title_signin'),
        ]);
    }

    public function postIndex(Request $request)
    {
        $rules = [
            'email' => 'required|email',
            'password' => 'required|min:8',
        ];

        if (B2b::VldFails($rules, $request->all())) {
            return B2b::Vld($rules, $request->all(), '/auth');
        }
        if (!in_array($request->get('guard'), ['company', 'user', 'admin'])) {
            return redirect('/auth');
        }

        if (Auth::guard('user')->attempt([
            'email' => $request->get('email'),
            'password' => $request->get('password'),
        ], $request->has('remember'))) {
            $user = Auth::guard('u')->user();
            switch ($user->status) {
            case 'suspend':
                Flash::error(trans('auth.user_suspend'));
                Auth::logout();

                return redirect('/auth');
                break;

            case 'pendig':
                Flash::error(trans('auth.user_pending'));
                Auth::guard('u')->logout();

                return redirect('/auth');
                break;

                case 'deny':
                    Flash::error(trans('auth.user_deny'));
                    Auth::guard('u')->logout();

                    return redirect('/auth');
                    break;

                    case 'confirmed':
                        Flash::error(trans('auth.user_confirmed'));
                        Auth::guard('u')->logout();

                        return redirect('/auth');
                        break;
            }

            return redirect('/dashboard');
        } elseif (Auth::guard('company')->attempt([
            'email' => $request->get('email'),
            'password' => $request->get('password'),
        ], $request->has('remember'))) {
            $user = Auth::guard('company')->user();
            switch ($user->status) {
          case 'suspend':
              Flash::error(trans('auth.user_suspend'));
              Auth::logout();

              return redirect('/auth');
              break;

          case 'suspend':
              Flash::error(trans('auth.user_pending'));
              Auth::guard('company')->logout();

              return redirect('/auth');
              break;
          }

            return redirect('/dashboard');
        }

        Flash::error(trans('auth.email_password_wrong'));

        return redirect('/auth');
    }

    public function getSignup()
    {
        return view('auth/signup_user', [
            'title' => trans('auth.title_singup'),
        ]);
    }

    public function postSignup(Request $request)
    {
        $rules = [
            'lname' => 'required|between:2,15',
            'fname' => 'required|between:2,10',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|between:8,16|confirmed',
            'password_confirmation' => 'required',
        ];

        if (B2b::VldFails($rules, $request->all())) {
            return B2b::Vld($rules, $request->all(), '/auth/signup/user');
        }

        $user = new User();
        $user->first_name = $request->get('fname');
        $user->last_name = $request->get('lname');
        $user->email = $request->get('email');
        $user->password = bcrypt($request->get('password'));
        $user->status = 'pending';
        if ($user->register()) {
            return redirect('/auth');
        }

        return redirect('/auth/signup/user');
    }

    public function getConfirmation($email, $activation)
    {
        $rules = [
            'activation' => 'required',
            'email' => 'required|email',
        ];

        if (B2b::VldFails($rules, compact('email', 'activation'))) {
            return B2b::Vld($rules, compact('email', 'activation'), '/auth');
        }

        $user = User::where('email', $email)->get();
        if (!sizeof($user)) {
            Flash::error(trans('auth.user_not_found'));

            return redirect('/auth');
        }
        $user = $user[0];

        if ($user->confirmation($activation)) {
            $auth = Auth::guard('user')->login($user);

            return redirect('/');
        }

        return redirect('/auth');
    }

    public function getReconfirmation()
    {
        return view('auth/reconfirmation', array(
            'title' => trans('auth.title_reconfirmation'),
        ));
    }

    public function postReconfirmation(Request $request)
    {
        $rules = [
            'email' => 'required|email',
        ];

        if (B2b::VldFails($rules, $request->all())) {
            return B2b::Vld($rules, $request->all(), '/auth/reconfirmation');
        }

        $user = User::where('email', $request->get('email'))->get();
        if (!sizeof($user)) {
            Flash::error(trans('auth.user_not_found'));

            return redirect('/auth/reconfirmation');
        }
        $user = $user[0];

        $user->reconfirmation();

        return redirect('/auth/reconfirmation');
    }

    public function getReset()
    {
        return view('/auth/reset', array(
            'title' => trans('auth.title_reminder'),
        ));
    }
    public function postReset(Request $request)
    {
        $rules = [
            'email' => 'required|email',
        ];

        if (B2b::VldFails($rules, $request->all())) {
            return B2b::Vld($rules, $request->all(), '/auth/reset');
        }
        $user = User::where('email', $request->get('email'))->get();
        if (!sizeof($user)) {
            Flash::error(trans('auth.user_not_found'));

            return redirect('/auth/reset');
        }
        $user = $user[0];
        if ($user->reset()) {
            return redirect('/auth');
        }

        return redirect('/auth/reset');
    }

    public function getPassword($email, $reset)
    {
        $rules = [
            'reset' => 'required',
            'email' => 'required|email',
        ];

        if (B2b::VldFails($rules, compact('email', 'reset'))) {
            return B2b::Vld($rules, compact('email', 'activation'), '/auth/reset');
        }

        $user = User::where('email', $email)->get();
        if (!sizeof($user)) {
            Flash::error(trans('auth.user_not_found'));

            return redirect('/auth/reset');
        }
        $user = $user[0];
        if ($user->user_reset == $reset) {
            return view('auth/password', array(
                'title' => trans('auth.title_change_password'),
                'email' => $email,
                'reset' => $reset,
            ));
        }

        Flash::error(trans('auth.reset_code_wrong'));

        return redirect('/auth/reset');
    }

    public function postPassword(Request $request)
    {
        $rules = [
            'email' => 'required',
            'reset' => 'required',
            'password' => 'required|between:8,16|confirmed',
            'password_confirmation' => 'required',
        ];

        if (B2b::VldFails($rules, $request->all())) {
            return B2b::Vld($rules, $request->all(), '/auth/reset');
        }

        $user = User::where('email', $request->get('email'))->get();
        if (!sizeof($user)) {
            Flash::error(trans('auth.user_not_found'));

            return redirect('/auth/reset');
        }
        $user = $user[0];
        if ($user->user_reset == $reset) {
            $user->password = bcrypt($request->get('password'));
            $user->user_reset = '';
            $user->save();

            Flash::success('your password changed with successe . now you can login to system .');

            return redirect('/auth');
        }

        Flash::error(trans('auth.reset_code_wrong'));

        return redirect('/auth/reset');
    }

    // Company
    public function getCompany()
    {
        $scopes = Scope::all();
        $groups = Group::all();

        return view('auth.signup_company', [
            'title' => trans('auth.title_signup_company'),
            'scopes' => $scopes,
            'groups' => $groups,
        ]);
    }

    public function postCompany(Request $request)
    {
        $rules = [
            'name' => 'required|between:2,40',
            'owner' => 'required|between:2,20',
            'email' => 'required|email|unique:companies,email',
            'group' => 'required',
            'phone' => 'required',
            'password' => 'required|between:8,16|confirmed',
            'password_confirmation' => 'required',
            'scopes' => 'required|array',
        ];

        if (B2b::VldFails($rules, $request->all())) {
            return B2b::Vld($rules, $request->all(), '/auth/company');
        }

        $Company = new Company();
        $Company->name = $request->get('name');
        $Company->owner = $request->get('owner');
        $Company->email = $request->get('email');
        $Company->group = $request->get('group');
        $Company->phone = $request->get('phone');
        $Company->plan = 'free';
        $Company->password = bcrypt($request->get('password'));
        $Company->scopes()->attach($request->get('scopes'));
        if ($Company->register()) {
            return redirect('/auth');
        }

        return redirect('/auth/company');
    }

    public function getCompanyconfirmation($email, $activation)
    {
        $rules = [
            'activation' => 'required',
            'email' => 'required|email',
        ];

        if (B2b::VldFails($rules, compact('email', 'activation'))) {
            return B2b::Vld($rules, compact('email', 'activation'), '/auth');
        }

        $Company = User::where('email', $email)->get();
        if (!sizeof($Company)) {
            Flash::error(trans('auth.comapny_not_found'));

            return redirect('/auth');
        }
        $Company = $Company[0];

        if ($Company->confirmation($activation)) {
            $auth = Auth::guard('company')->login($Company);

            return redirect('/');
        }

        return redirect('/auth');
    }

    public function getCompanyReconfirmation()
    {
        return view('auth/company_reconfirmation', array(
            'title' => trans('auth.title_company_reconfirmation'),
        ));
    }

    public function postCompanyreconfirmation(Request $request)
    {
        $rules = [
            'email' => 'required|email',
        ];

        if (B2b::VldFails($rules, $request->all())) {
            return B2b::Vld($rules, $request->all(), '/auth/company_reconfirmation');
        }

        $company = Company::where('email', $request->get('email'))->get();
        if (!sizeof($company)) {
            Flash::error(trans('auth.company_not_found'));

            return redirect('/auth/company_reconfirmation');
        }
        $company = $company[0];

        if ($company->reconfirmation()) {
            return redirect('/auth/company_reconfirmation');
        }

        return redirect('/auth/company_reconfirmation');
    }

    public function getCompanyreset()
    {
        return view('/auth/company/reset', array(
            'title' => trans('auth.title_company_reminder'),
        ));
    }

    public function postCompanyreset(Request $request)
    {
        $rules = [
            'email' => 'required|email',
        ];

        if (B2b::VldFails($rules, $request->all())) {
            return B2b::Vld($rules, $request->all(), '/auth/company_reset');
        }
        $company = Company::where('email', $request->get('email'))->get();
        if (!sizeof($company)) {
            Flash::error(trans('auth.company_not_found'));

            return redirect('/auth/company_reset');
        }
        $company = $company[0];
        if ($company->reset()) {
            return redirect('/auth');
        }

        return redirect('/auth/company_reset');
    }

    public function getCompanypassword($email, $reset)
    {
        $rules = [
            'reset' => 'required',
            'email' => 'required|email',
        ];

        if (B2b::VldFails($rules, compact('email', 'reset'))) {
            return B2b::Vld($rules, compact('email', 'activation'), '/auth/company_reset');
        }

        $company = Company::where('email', $email)->get();
        if (!sizeof($company)) {
            Flash::error(trans('auth.company_not_found'));

            return redirect('/auth/company_reset');
        }
        $company = $company[0];
        if ($company->company_reset == $reset) {
            return view('auth/company_password', array(
                'title' => trans('auth.title_company_change_password'),
                'email' => $email,
                'reset' => $reset,
            ));
        }

        Flash::error(trans('auth.reset_code_wrong'));

        return redirect('/auth/company_reset');
    }

    public function postCompanypassword(Request $request)
    {
        $rules = [
            'email' => 'required',
            'reset' => 'required',
            'password' => 'required|between:8,16|confirmed',
            'password_confirmation' => 'required',
        ];

        if (B2b::VldFails($rules, $request->all())) {
            return B2b::Vld($rules, $request->all(), '/auth/reset');
        }

        $company = Company::where('email', $request->get('email'))->get();
        if (!sizeof($company)) {
            Flash::error(trans('auth.company_not_found'));

            return redirect('/auth/reset');
        }
        $company = $company[0];
        if ($company->company_reset == $reset) {
            $company->password = bcrypt($request->get('password'));
            $company->company_reset = '';
            $company->save();

            Flash::success('auth.success_password_change');

            return redirect('/auth');
        }

        Flash::error(trans('auth.reset_code_wrong'));

        return redirect('/auth/company_reset');
    }

    public function getSignout()
    {
        foreach (['company', 'user', 'admin'] as $guard) {
            $auth = Auth::guard($guard)->logout();
        }

        return redirect('/auth');
    }

    //  admins
    public function getAdmin()
    {
        return view('auth.admin', array(
            'title' => 'ورود مدیر سیستم',
        ));
    }
}
