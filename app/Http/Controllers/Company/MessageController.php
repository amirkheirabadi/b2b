<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class MessageController extends Controller
{
    public function getIndex(Request $request)
    {
      switch ($request->get('guard')) {
  			case 'user':
  				return view('message.user_index',array(
  					'title' => 'مدیریت پیام ها',
  					  'user'=> $request->get('user')
  				));
  				break;

  			case 'company':

  				return view('message.company_index',array(
  					'title' => 'مدیریت پیام ها',
  					'company' => $request->get('company'),
  				));
  				break;

  				case 'admin':
  				return view('message.admin_index',array(
  					'title' => 'مدیریت پیام ها',
  						'admin'=> $request->get('admin')
  				));
  					break;
  		}
    }
}
