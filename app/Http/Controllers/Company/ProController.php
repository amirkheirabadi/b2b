<?php

namespace App\Http\Controllers;

use App\Http\Helper\B2b;
use App\User;
use Flash;
use App\Price;
use Illuminate\Http\Request;
use App\Product;

class ProController extends Controller
{
    public function getIndex(Request $request)
    {
        $title = 'مدیریت محصولات';

        switch ($request->get('guard')) {

            case 'company':
                $prices_q = Price::all();
                $prices = [];
                foreach ($prices_q as $q) {
                    $prices[$q->name] = $q->rial;
                }

                return view('product.company_index', array(
                    'title' => 'صفحه داشبورد',
                    'company' => $request->get('company'),
                    'price' => $prices,
                ));
              break;

              case 'admin':
                return view('product.admin_index', array(
                    'title' => 'صفحه داشبورد',
                        'admin' => $request->get('admin'),
                ));
              break;
        }
    }

    public function postIndex(Request $request)
    {
        $order = $request->get('order')[0];
        $order_direction = $order['dir'];
        switch ($order['column']) {
        case '0':
            $order_item = 'id';
            break;

        case '1':
            $order_item = 'name';
            break;

        case '2':
            $order_item = 'company';
            break;

        case '3':
                $order_item = 'description';
          break;

        default:
        $order_item = 'id';
            break;
        }
        $data = [];

        if ($request->get('guard') == 'admin') {
            $products = Product::where(function ($query) use ($request) {
                $query->where('name', 'LIKE', '%'.$request->get('search')['value'].'%');
            })->orderBy($order_item, $order_direction)->limit($request->get('length'))->skip($request->get('start'))->get();
        } else {
            $products = Product::where('company_id', $request->get('company')['id'])->where(function ($query) use ($request) {
                $query->where('name', 'LIKE', '%'.$request->get('search')['value'].'%');
            })->orderBy($order_item, $order_direction)->limit($request->get('length'))->skip($request->get('start'))->get();
        }

        foreach ($products as $product) {
            array_push($data, array(
                'name' => $product->name,
                'id' => $product->id,
                'company' => $product->company->name,
                'description' => $product->description,
                'action' => '',
            ));
        }

        $user_count = Product::where(function ($query) use ($request) {
            $query->where('name', 'LIKE', '%'.$request->get('search')['value'].'%');
        })->count();

        return array('draw' => $request->get('draw'),
            'recordsTotal' => $user_count,
            'recordsFiltered' => $user_count,
            'data' => $data, );
    }

    public function getCreate()
    {
        $title = 'user create';

        return view('user.create', compact('title'));
    }

    public function postCreate()
    {
        $rules = [
            'first_name' => 'required|between:2,15',
            'last_name' => 'required|between:2,10',
            'email' => 'required|email|unique:users,email',
            'password' => 'between:8,16|confirmed',
            'mobile' => 'required',
            'status' => 'required',
        ];

        if (B2b::VldFails($rules, $request->all())) {
            return B2b::Vld($rules, $request->all(), '/users/create');
        }

        $user = new User();
        $user->first_name = $request->get('first_name');
        $user->last_name = $request->get('last_name');
        $user->email = $request->get('email');
        $user->status = $request->get('status');
        $user->mobile = $request->get('mobile');

        $user->password = bcrypt($request->get('password'));

        $user->save();
        Flash::success('user create success ..');

        return redirect('/users');
    }

    public function getEdit($id)
    {
        $user = User::find($id);

        if (!$user) {
            Flash::error('user not found');
        }

        $title = 'edit user';

        return view('user.edit', compact('title', 'user'));
    }

    public function postEdit($id)
    {
        $rules = [
            'first_name' => 'required|between:2,15',
            'last_name' => 'required|between:2,10',
            'email' => 'required|email|unique:users,email',
            'password' => 'between:8,16|confirmed',
            'mobile' => 'required',
            'status' => 'required',
        ];

        if (B2b::VldFails($rules, $request->all())) {
            return B2b::Vld($rules, $request->all(), '/users/edit/'.$id);
        }

        $user = User::find($id);

        if (!$user) {
            Flash::error('user not found');
        }

        $user = new User();
        $user->first_name = $request->get('first_name');
        $user->last_name = $request->get('last_name');
        $user->email = $request->get('email');
        $user->status = $request->get('status');
        $user->mobile = $request->get('mobile');

        if ($request->get('password')) {
            $user->password = bcrypt($request->get('password'));
        }

        $user->save();
        Flash::success('user create success ..');

        return redirect('/users/edit/'.$id);
    }

    public function getDestroy($id)
    {
        # code...
    }

    public function getShow($id)
    {
        # code...
    }
}
