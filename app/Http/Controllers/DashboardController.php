<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Price;
use App\Helper\Payline;
use App\Transaction;
use App\Helper\B2b;
use App\User;
use App\Admin;

class DashboardController extends Controller
{
    public function getIndex(Request $request)
    {
        switch ($request->get('guard')) {
            case 'user':
                return view('dashboard.user_index', array(
                    'title' => 'صفحه داشبورد',
                      'user' => $request->get('user'),
                ));
                break;

            case 'company':
                $prices_q = Price::all();
                $prices = [];
                foreach ($prices_q as $q) {
                    $prices[$q->name] = $q->rial;
                }

                return view('dashboard.company_index', array(
                    'title' => 'صفحه داشبورد',
                    'company' => $request->get('company'),
                    'price' => $prices,
                ));
                break;

                case 'admin':
                return view('dashboard.admin_index', array(
                    'title' => 'صفحه داشبورد',
                        'admin' => $request->get('admin'),
                ));
              break;
        }
    }

    public function getProfile(Request $request)
    {
        switch ($request->get('guard')) {
            case 'user':
                return view('dashboard.profile_customer', array(
                    'title' => 'پروفایل کاربری',
                    'user' => $request->get('user'),
                ));
            break;

            case 'company':
                return view('dashboard.profile_company', array(
                    'title' => 'پروفایل کاربری',
                    'user' => $request->get('user'),
                ));
            break;

            case 'admin':
            return view('dashboard.profile_admin', array(
                'title' => 'پروفایل کاربری',
                'admin' => $request->get('admin'),
            ));
            break;
        }
    }

    public function postProfile(Request $request)
    {
        switch ($request->get('guard')) {
            case 'user':

            $rules = [
                'first_name' => 'required|between:2,15',
                'last_name' => 'required|between:2,10',
                'mobile' => 'required',
                'email' => 'required|email',
                'password' => 'between:8,16|confirmed',
            ];

            if (B2b::VldFails($rules, $request->all())) {
                return B2b::Vld($rules, $request->all(), '/dashboard/profile');
            }

            $user = User::find($request->get('user')['id']);
            $user->first_name = $request->get('first_name');
            $user->last_name = $request->get('last_name');
            $user->email = $request->get('email');
            $user->mobile = $request->get('mobile');

            if ($request->get('password')) {
                $user->password = bcrypt($required->get('password'));
            }

            $user->save();

            return redirect('/dashboard/profile');
                break;

                case 'admin':
                $rules = [
                    'first_name' => 'required|between:2,15',
                    'last_name' => 'required|between:2,10',
                    'email' => 'required|email',
                    'password' => 'between:8,16|confirmed',
                ];

                if (B2b::VldFails($rules, $request->all())) {
                    return B2b::Vld($rules, $request->all(), '/dashboard/profile');
                }

                $admin = Admin::find($request->get('admin')['id']);
                $admin->first_name = $request->get('first_name');
                $admin->last_name = $request->get('last_name');
                $admin->email = $request->get('email');

                if ($request->get('password')) {
                    $admin->password = bcrypt($required->get('password'));
                }

                $admin->save();

                return redirect('/dashboard/profile');
                  break;
        }
    }

    public function postAccount(Request $request)
    {
        $price = Price::where('name', $request->get('plan'))->get();
        $price = $price[0];
        $total = $price['rial'];

        $trans = new Transaction();
        $trans->owner = $request->get('company')['id'];
        $trans->price = $total;
        $trans->plan = $request->get('plan');
        $trans->status = 'pending';
        $trans->save();

        $payline = new Payline($trans->id);

        return redirect($payline->pay());
    }
}
