<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin;
use URL;

class AdminController extends Controller
{
    public function getIndex(Request $request)
    {
        return view('admin/index', [
          'title' => 'مدیریت مدیران سیستم',
          'admin' => $request->get('admin'),
        ]);
    }

    public function postIndex(Request $request)
    {
        $order = $request->get('order')[0];
        $order_direction = $order['dir'];
        switch ($order['column']) {
      case '0':
          $order_item = 'first_name';
          break;

      case '1':
          $order_item = 'email';
          break;

      case '2':
          $order_item = 'status';
          break;

      default:
      $order_item = 'id';
          break;
      }
        $data = [];

        $admins = Admin::where(function ($query) use ($request) {
              $query->where('first_name', 'LIKE', '%'.$request->get('search')['value'].'%')
              ->orWhere('last_name', 'LIKE', '%'.$request->get('search')['value'].'%')
              ->orWhere('email', 'LIKE', '%'.$request->get('search')['value'].'%');
          })->orderBy($order_item, $order_direction)->limit($request->get('length'))->skip($request->get('start'))->get();

        foreach ($admins as $admin) {
            switch ($admin->status) {
            case 'suspend':
              $status = 'مسدود';
              break;

            case 'active':
            $status = 'فعال';
              break;
          }

            array_push($data, array(
              'name' => $admin->first_name.' '.$admin->last_name,
              'email' => $admin->email,
              'status' => $status,
              'action' => '<a class="data_btn" href='.URL::to('/dashboard/admins/edit/'.$admin->id).'><i class="fa fa-pencil-square-o"></i></a><a class="data_btn confirmation" href='.URL::to('/dashboard/admins/destroy/'.$admin->id).'><i class="fa fa-trash-o"></i></a>',
          ));
        }

        $admin_count = Admin::where(function ($query) use ($request) {
              $query->where('first_name', 'LIKE', '%'.$request->get('search')['value'].'%')
              ->orWhere('last_name', 'LIKE', '%'.$request->get('search')['value'].'%')
              ->orWhere('email', 'LIKE', '%'.$request->get('search')['value'].'%');
          })->count();

        return array('draw' => $request->get('draw'),
          'recordsTotal' => $admin_count,
          'recordsFiltered' => $admin_count,
          'data' => $data, );
    }

    public function getCreate()
    {
        $title = 'افزودن مدیر جدید';

        return view('admin.create', compact('title'));
    }

    public function postCreate()
    {
        $rules = [
            'first_name' => 'required|between:2,15',
            'last_name' => 'required|between:2,10',
            'email' => 'required|email|unique:admins,email',
            'password' => 'between:8,16|confirmed',
            'status' => 'required',
        ];

        if (B2b::VldFails($rules, $request->all())) {
            return B2b::Vld($rules, $request->all(), '/admins/create');
        }

        $admin = new Admin();
        $admin->first_name = $request->get('first_name');
        $admin->last_name = $request->get('last_name');
        $admin->email = $request->get('email');
        $admin->status = $request->get('status');

        $admin->password = bcrypt($request->get('password'));

        $admin->save();
        Flash::success('مدیر جدید با موفقیت ایجاد شد .');

        return redirect('/admins');
    }

    public function getEdit($id)
    {
        $admin = Admin::find($id);

        if (!$admin) {
            Flash::error('مدیری با این مشخصات در سیستم یافت نشد .');

            return rediret('/dashboard/admins');
        }
        $title = 'ویرایش اطلاعات مدیر';

        return view('admin.edit', compact('title', 'admin'));
    }

    public function postEdit($id)
    {
        $rules = [
            'first_name' => 'required|between:2,15',
            'last_name' => 'required|between:2,10',
            'email' => 'required|email|unique:admins,email',
            'password' => 'between:8,16|confirmed',
            'mobile' => 'required',
            'status' => 'required',
        ];

        if (B2b::VldFails($rules, $request->all())) {
            return B2b::Vld($rules, $request->all(), '/admins/edit/'.$id);
        }

        $admin = Admin::find($id);

        if (!$admin) {
            Flash::error('مدیری با این مشخصات در سیستم یافت نشد .');

            return rediret('/dashboard/admins');
        }

        $admin->first_name = $request->get('first_name');
        $admin->last_name = $request->get('last_name');
        $admin->email = $request->get('email');
        $admin->status = $request->get('status');

        if ($request->get('password')) {
            $admin->password = bcrypt($request->get('password'));
        }

        $admin->save();
        Flash::success('اطلاعات مدیر با موفقیت ویرایش شد .');

        return redirect('/admins/edit/'.$id);
    }

    public function getDestroy($id, Request $request)
    {
        $admin = Admin::where('id', $id)->where('id', '!=', $request->get('admin')['id'])->get();

        if (!sizeof($admin)) {
            Flash::error('مدیری با این مشخصات در سیستم یافت نشد .');

            return rediret('/dashboard/admins');
        }
        $admin = $admin[0];
        $admin->delete();

        Flash::success('مدیر با موفقیت حذف شد .');

        return redirect('/admins');
    }
}
