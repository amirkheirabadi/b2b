<?php

namespace App\Http\Controllers\Admin;

use App\Http\Helper\B2b;
use App\User;
use Flash;
use Illuminate\Http\Request;
use App\Product;
use App\Http\Controllers\Controller;
use URL;

class ProController extends Controller
{
    public function getIndex(Request $request)
    {
        $title = 'مدیریت محصولات';

        return view('product.admin.index', array(
            'title' => 'صفحه داشبورد',
                'admin' => $request->get('admin'),
        ));
    }

    public function postIndex(Request $request)
    {
        $order = $request->get('order')[0];
        $order_direction = $order['dir'];
        switch ($order['column']) {
        case '0':
            $order_item = 'id';
            break;

        case '1':
            $order_item = 'name';
            break;

        case '2':
            $order_item = 'company';
            break;

        case '3':
                $order_item = 'description';
          break;

        default:
        $order_item = 'id';
            break;
        }
        $data = [];

        if ($request->get('guard') == 'admin') {
            $products = Product::where(function ($query) use ($request) {
                $query->where('name', 'LIKE', '%'.$request->get('search')['value'].'%');
            })->orderBy($order_item, $order_direction)->limit($request->get('length'))->skip($request->get('start'))->get();
        } else {
            $products = Product::where('company_id', $request->get('company')['id'])->where(function ($query) use ($request) {
                $query->where('name', 'LIKE', '%'.$request->get('search')['value'].'%');
            })->orderBy($order_item, $order_direction)->limit($request->get('length'))->skip($request->get('start'))->get();
        }

        foreach ($products as $product) {
            array_push($data, array(
                'name' => $product->name,
                'id' => $product->id,
                'company' => $product->company->name,
                'description' => $product->description,
                'action' => '<a class="data_btn" href='.URL::to('/dashboard/products/edit/'.$product->id).'><i class="fa fa-pencil-square-o"></i></a><a class="data_btn confirmation" href='.URL::to('/dashboard/products/destroy/'.$product->id).'><i class="fa fa-trash-o"></i></a>',
            ));
        }

        $user_count = Product::where(function ($query) use ($request) {
            $query->where('name', 'LIKE', '%'.$request->get('search')['value'].'%');
        })->count();

        return array('draw' => $request->get('draw'),
            'recordsTotal' => $user_count,
            'recordsFiltered' => $user_count,
            'data' => $data, );
    }

    public function getEdit($id)
    {
        $pro = Product::find($id);

        if (!$pro) {
            Flash::error('Product not found');
        }

        $title = 'Edit product';

        return view('product.admin.edit', compact('title', 'pro'));
    }

    public function postEdit($id)
    {
        $rules = [
            'first_name' => 'required|between:2,15',
            'last_name' => 'required|between:2,10',
            'email' => 'required|email|unique:users,email',
            'password' => 'between:8,16|confirmed',
            'mobile' => 'required',
            'status' => 'required',
        ];

        if (B2b::VldFails($rules, $request->all())) {
            return B2b::Vld($rules, $request->all(), '/users/edit/'.$id);
        }

        $user = User::find($id);

        if (!$user) {
            Flash::error('user not found');
        }

        $user = new User();
        $user->first_name = $request->get('first_name');
        $user->last_name = $request->get('last_name');
        $user->email = $request->get('email');
        $user->status = $request->get('status');
        $user->mobile = $request->get('mobile');

        if ($request->get('password')) {
            $user->password = bcrypt($request->get('password'));
        }

        $user->save();
        Flash::success('user create success ..');

        return redirect('/users/edit/'.$id);
    }

    public function getDestroy($id)
    {
        # code...
    }

    public function getShow($id)
    {
        # code...
    }
}
