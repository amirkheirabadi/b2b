<?php

namespace App\Http\Controllers\Admin;

use App\Http\Helper\B2b;
use App\Order;
use Flash;
use Illuminate\Http\Request;
use URL;

class OrderController extends \App\Http\Controllers\Controller
{
    public function getIndex(Request $request)
    {
        $title = 'مدیریت سفارشات';
        $user = $request->get('user');

        return view('order.index', compact('title', 'user'));
    }

    public function postIndex(Request $request)
    {
        $order = $request->get('order')[0];
        $order_direction = $order['dir'];
        switch ($order['column']) {
        case '0':
            $order_item = 'id';
            break;

        case '1':
            $order_item = 'product_id';
            break;

        case '3':
            $order_item = 'created_at';
            break;

        default:
            $order_item = 'created_at';
            break;
        }
        $data = [];

        $orders = Order::where(function ($query) use ($request) {
            $query->where('id', 'LIKE', '%'.$request->get('search')['value'].'%');
        })->orderBy($order_item, $order_direction)->limit($request->get('length'))->skip($request->get('start'))->get();

        foreach ($orders as $order) {
            switch ($order->status) {
                case 'pending':
                $status = 'خوانده نشده';
                        break;

                case 'awaiting':
                $status = 'در جریان';
                    break;

                    case 'success':
                    $status = 'موفقیت آمیز';
                        break;

      case 'field':
      $status = 'ناموفق';
        break;
            }

            array_push($data, array(
                'id' => $order->id,
                'product' => $order->product->name,
                'company' => $order->product->company->name,
                'date' => $order->created_at->toFormattedDateString(),
                'action' => '<a class="data_btn" href='.URL::to('/dashboard/orders/edit/'.$order->id).'><i class="fa fa-pencil-square-o"></i></a><a class="data_btn confirmation" href='.URL::to('/dashboard/orders/destroy/'.$order->id).'><i class="fa fa-trash-o"></i></a>',
            ));
        }

        $order_count = Order::where(function ($query) use ($request) {
      $query->where('id', 'LIKE', '%'.$request->get('search')['value'].'%');
        })->count();

        return array('draw' => $request->get('draw'),
            'recordsTotal' => $order_count,
            'recordsFiltered' => $order_count,
            'data' => $data, );
    }

    public function postCreate()
    {
        $rules = [
            'first_name' => 'required|between:2,15',
            'last_name' => 'required|between:2,10',
            'email' => 'required|email|unique:users,email',
            'password' => 'between:8,16|confirmed',
            'mobile' => 'required',
            'status' => 'required',
        ];

        if (B2b::VldFails($rules, $request->all())) {
            return B2b::Vld($rules, $request->all(), '/users/create');
        }

        $user = new User();
        $user->first_name = $request->get('first_name');
        $user->last_name = $request->get('last_name');
        $user->email = $request->get('email');
        $user->status = $request->get('status');
        $user->mobile = $request->get('mobile');

        $user->password = bcrypt($request->get('password'));

        $user->save();
        Flash::success('user create success ..');

        return redirect('/users');
    }
}
