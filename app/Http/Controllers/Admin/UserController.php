<?php

namespace App\Http\Controllers\Admin;

use App\Http\Helper\B2b;
use App\User;
use Flash;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use URL;
use App\Order;

class UserController extends Controller
{
    public function getIndex(Request $request)
    {
        return view('user.index', [
            'title' => 'مدیریت کاربران',
        ]);
    }

    public function postIndex(Request $request)
    {
        $order = $request->get('order')[0];
        $order_direction = $order['dir'];
        switch ($order['column']) {
        case '0':
            $order_item = 'first_name';
            break;

        case '1':
            $order_item = 'email';
            break;

        case '2':
            $order_item = 'mobile';
            break;

        case '2':
            $order_item = 'status';
            break;

        default:
            $order_item = 'created_at';
            break;
        }
        $data = [];

        $users = User::where(function ($query) use ($request) {
            $query->where('first_name', 'LIKE', '%'.$request->get('search')['value'].'%')
            ->orWhere('last_name', 'LIKE', '%'.$request->get('search')['value'].'%')
            ->orWhere('email', 'LIKE', '%'.$request->get('search')['value'].'%')
            ->orWhere('mobile', 'LIKE', '%'.$request->get('search')['value'].'%');
        })->orderBy($order_item, $order_direction)->limit($request->get('length'))->skip($request->get('start'))->get();

        foreach ($users as $user) {
            switch ($user->status) {
                case 'pendig':
                $status = 'در انتظار فعالسازی';
                        break;

                case 'active':
                $status = 'فعال';
                    break;

                    case 'suspend':
                    $status = 'مسدود';
                        break;
            }

            array_push($data, array(
                'name' => $user->first_name.' '.$user->last_name,
                'mobile' => $user->mobile,
                'status' => $status,
                'action' => '<a class="data_btn" href='.URL::to('/dashboard/users/edit/'.$user->id).'><i class="fa fa-pencil-square-o"></i></a><a class="data_btn confirmation" href='.URL::to('/dashboard/users/destroy/'.$user->id).'><i class="fa fa-trash-o"></i></a>',
            ));
        }

        $user_count = User::where(function ($query) use ($request) {
            $query->where('first_name', 'LIKE', '%'.$request->get('search')['value'].'%')
            ->orWhere('last_name', 'LIKE', '%'.$request->get('search')['value'].'%')
            ->orWhere('email', 'LIKE', '%'.$request->get('search')['value'].'%')
            ->orWhere('mobile', 'LIKE', '%'.$request->get('search')['value'].'%');
        })->count();

        return array('draw' => $request->get('draw'),
            'recordsTotal' => $user_count,
            'recordsFiltered' => $user_count,
            'data' => $data, );
    }

    public function getCreate()
    {
        $title = 'افزودن کاربر جدید';

        return view('user.create', compact('title'));
    }

    public function postCreate()
    {
        $rules = [
            'first_name' => 'required|between:2,15',
            'last_name' => 'required|between:2,10',
            'email' => 'required|email|unique:users,email',
            'password' => 'between:8,16|confirmed',
            'mobile' => 'required',
            'status' => 'required',
        ];

        if (B2b::VldFails($rules, $request->all())) {
            return B2b::Vld($rules, $request->all(), '/users/create');
        }

        $user = new User();
        $user->first_name = $request->get('first_name');
        $user->last_name = $request->get('last_name');
        $user->email = $request->get('email');
        $user->status = $request->get('status');
        $user->mobile = $request->get('mobile');

        $user->password = bcrypt($request->get('password'));

        $user->save();
        Flash::success('کاربر جدید با موفقیت ایجاد شد .');

        return redirect('/users');
    }

    public function getEdit($id)
    {
        $user = User::find($id);

        if (!$user) {
            Flash::error('کاربری با این مشخصات در سیستم یافت نشد .');

            return rediret('/dashboard/users');
        }
        $title = 'ویرایش اطلاعات کاربر';

        return view('user.edit', compact('title', 'user'));
    }

    public function postEdit($id)
    {
        $rules = [
            'first_name' => 'required|between:2,15',
            'last_name' => 'required|between:2,10',
            'email' => 'required|email|unique:users,email',
            'password' => 'between:8,16|confirmed',
            'mobile' => 'required',
            'status' => 'required',
        ];

        if (B2b::VldFails($rules, $request->all())) {
            return B2b::Vld($rules, $request->all(), '/users/edit/'.$id);
        }

        $user = User::find($id);

        if (!$user) {
            Flash::error('کاربری با این مشخصات در سیستم یافت نشد .');

            return rediret('/dashboard/users');
        }

        $user->first_name = $request->get('first_name');
        $user->last_name = $request->get('last_name');
        $user->email = $request->get('email');
        $user->status = $request->get('status');
        $user->mobile = $request->get('mobile');

        if ($request->get('password')) {
            $user->password = bcrypt($request->get('password'));
        }

        $user->save();
        Flash::success('اطلاعات کاربر با موفقیت ویرایش شد .');

        return redirect('/users/edit/'.$id);
    }

    public function getDestroy($id)
    {
        $user = User::find($id);

        if (!$user) {
            Flash::error('کاربری با این مشخصات در سیستم یافت نشد .');

            return rediret('/dashboard/users');
        }

        Order::where('owner', $user->id)->update(['woner', 0]);
        $user->delete();

        Flash::success('کاربر با موفقیت حذف شد .');

        return redirect('/users');
    }
}
