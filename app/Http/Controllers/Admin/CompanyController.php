<?php

namespace App\Http\Controllers\Admin;

use App\Http\Helper\B2b;
use App\User;
use Flash;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Company;
use URL;
use App\Group;
use App\Scope;

class CompanyController extends Controller
{
    public function getIndex()
    {
        $title = 'مدیریت شرکت ها';

        return view('company.admin.index', compact('title'));
    }

    public function postIndex(Request $request)
    {
        $order = $request->get('order')[0];
        $order_direction = $order['dir'];
        switch ($order['column']) {
        case '0':
            $order_item = 'name';
            break;

      case '1':
        $order_item = 'owner';
        break;

        case '2':
            $order_item = 'email';
            break;

        case '3':
            $order_item = 'status';
            break;

        default:
            $order_item = 'created_at';
            break;
        }
        $data = [];

        $companies = Company::where(function ($query) use ($request) {
            $query->where('name', 'LIKE', '%'.$request->get('search')['value'].'%')
            ->orWhere('owner', 'LIKE', '%'.$request->get('search')['value'].'%')
            ->orWhere('email', 'LIKE', '%'.$request->get('search')['value'].'%')
            ->orWhere('mobile', 'LIKE', '%'.$request->get('search')['value'].'%');
        })->orderBy($order_item, $order_direction)->limit($request->get('length'))->skip($request->get('start'))->get();

        foreach ($companies as $company) {
            switch ($company->status) {
                case 'pending':
                $status = 'در انتظار فعال سازی';
                        break;

                case 'active':
                $status = 'در انتظار تایید';
                    break;

                    case 'suspend':
                    $status = 'مسدود';
                        break;

            case 'confirmed':
                    $status = 'تایید شده';
                        break;

              case 'deny':
                    $status = 'تایید نشده';
                    break;
                }

            array_push($data, array(
                'name' => $company->name,
                'owner' => $company->owner,
                'email' => $company->email,
                'mobile' => $company->mobile,
                'status' => $status,
                'action' => '<a class="data_btn" href='.URL::to('/dashboard/companies/edit/'.$company->id).'><i class="fa fa-pencil-square-o"></i></a><a class="data_btn confirmation" href='.URL::to('/dashboard/companies/destroy/'.$company->id).'><i class="fa fa-trash-o"></i></a>',
            ));
        }

        $company_count = Company::where(function ($query) use ($request) {
            $query->where('name', 'LIKE', '%'.$request->get('search')['value'].'%')
            ->orWhere('owner', 'LIKE', '%'.$request->get('search')['value'].'%')
            ->orWhere('email', 'LIKE', '%'.$request->get('search')['value'].'%')
            ->orWhere('mobile', 'LIKE', '%'.$request->get('search')['value'].'%');
        })->count();

        return array('draw' => $request->get('draw'),
            'recordsTotal' => $company_count,
            'recordsFiltered' => $company_count,
            'data' => $data, );
    }

    public function getCreate()
    {
        $title = 'افزودن شرکت جدید';
        $groups = Group::all();
        $scopes = Scope::all();

        return view('company.admin.create', compact('title', 'groups', 'scopes'));
    }

    public function postCreate()
    {
        $rules = [
            'name' => 'required|between:2,15',
            'owner' => 'required|between:2,10',
            'mobile' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'between:8,16|confirmed',
            'status' => 'required',
        ];

        if (B2b::VldFails($rules, $request->all())) {
            return B2b::Vld($rules, $request->all(), '/companies/create');
        }

        $user = new User();
        $user->name = $request->get('name');
        $user->owner = $request->get('owner');
        $user->email = $request->get('email');
        $user->mobile = $request->get('mobile');
        $user->status = $request->get('status');

        $user->password = bcrypt($request->get('password'));

        $user->save();
        Flash::success('Company create success ..');

        return redirect('/companies');
    }

    public function getEdit($id)
    {
        $company = Company::find($id);

        if (!$company) {
            Flash::error('Company not found');
        }

        $title = 'edit company';

        return view('company.edit', compact('title', 'company'));
    }

    public function postEdit($id)
    {
        $rules = [
            'first_name' => 'required|between:2,15',
            'last_name' => 'required|between:2,10',
            'email' => 'required|email|unique:users,email',
            'password' => 'between:8,16|confirmed',
            'mobile' => 'required',
            'status' => 'required',
        ];

        if (B2b::VldFails($rules, $request->all())) {
            return B2b::Vld($rules, $request->all(), '/users/edit/'.$id);
        }

        $user = User::find($id);

        if (!$user) {
            Flash::error('user not found');
        }

        $user = new User();
        $user->first_name = $request->get('first_name');
        $user->last_name = $request->get('last_name');
        $user->email = $request->get('email');
        $user->status = $request->get('status');
        $user->mobile = $request->get('mobile');

        if ($request->get('password')) {
            $user->password = bcrypt($request->get('password'));
        }

        $user->save();
        Flash::success('user create success ..');

        return redirect('/users/edit/'.$id);
    }

    public function getDestroy($id)
    {
        # code...
    }

    public function getShow($id)
    {
        # code...
    }
}
