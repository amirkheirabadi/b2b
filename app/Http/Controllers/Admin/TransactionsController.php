<?php

namespace App\Http\Controllers\Admin;

use App\Transaction;
use App\Http\Controllers\Controller;
use URL;
use Illuminate\Http\Request;

class TransactionsController extends Controller
{
    public function getIndex(Request $request)
    {
        return view('transactions.index', [
          'title' => 'مدیریت تراکنش ها',
      ]);
    }

    public function postIndex(Request $request)
    {
        $order = $request->get('order')[0];
        $order_direction = $order['dir'];
        switch ($order['column']) {
      case '0':
          $order_item = 'id';
          break;

      case '1':
          $order_item = 'owner';
          break;

      case '2':
          $order_item = 'price';
          break;

      case '3':
          $order_item = 'created_at';
          break;

      case '4':
        $order_item = 'status';
        break;

      default:
          $order_item = 'created_at';
          break;
      }
        $data = [];

        $transactions = Transaction::where(function ($query) use ($request) {
          $query->where('id', 'LIKE', '%'.$request->get('search')['value'].'%')
          ->orWhere('price', 'LIKE', '%'.$request->get('search')['value'].'%');
      })->orderBy($order_item, $order_direction)->limit($request->get('length'))->skip($request->get('start'))->get();

        foreach ($transactions as $transaction) {
            switch ($transaction->status) {
              case 'pending':
                $status = 'در انتظار پرداخت';
                break;

              case 'success':
                $status = 'پرداخت شده';
                break;
          }
            array_push($data, array(
              'id' => $transaction->id,
              'owner' => $transaction->company->name,
              'amount' => $transaction->price.' ریال',
              'date' => $transaction->created_at->toFormattedDateString(),
              'status' => $status,
              'action' => '<a class="data_btn" href='.URL::to('/dashboard/transactions/show/'.$transaction->id).'><i class="fa fa-eye"></i></a><a class="data_btn confirmation" href='.URL::to('/dashboard/transactions/destroy/'.$transaction->id).'><i class="fa fa-trash-o"></i></a>',
          ));
        }

        $trans_count = Transaction::where(function ($query) use ($request) {
          $query->where('id', 'LIKE', '%'.$request->get('search')['value'].'%')
          ->orWhere('price', 'LIKE', '%'.$request->get('search')['value'].'%');
      })->count();

        return array('draw' => $request->get('draw'),
          'recordsTotal' => $trans_count,
          'recordsFiltered' => $trans_count,
          'data' => $data, );
    }

    public function getShow($id)
    {
        $trans = Transaction::find($id);

        if (!$trans) {
            Flash::error('تراکنشی با این شناسه در سیستم قبت نشده است .');

            return redirect('/dashboard/transactions');
        }

        $title = 'نمایش تراکنش';

        return view('transactions.show', compact('title', 'trans'));
    }

    public function getDestroy($id)
    {
        $user = User::find($id);

        if (!$user) {
            Flash::error('کاربری با این مشخصات در سیستم یافت نشد .');

            return rediret('/dashboard/users');
        }

        Order::where('owner', $user->id)->update(['woner', 0]);
        $user->delete();

        Flash::success('کاربر با موفقیت حذف شد .');

        return redirect('/users');
    }
}
