<?php

namespace App\Http\Controllers;

use App\Group;
use App\Scope;
use App\Company;
use App\Product;
use App\Country;
use Session;
use App\Helper\B2b;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function getIndex()
    {
        $groups = Group::where('parent', 0)->get();
        $scopes = Scope::all();
        $countries = [];
        $all_countries = [];
        foreach ($scopes as $scope) {
            $co = Country::where('scope', $scope->id)->select('countryCode')->get();
            $countries[$scope->slug] = [];
            foreach ($co as $country) {
                $countries[$scope->slug][$country['countryCode']] = '#81D5D3';
                $all_countries[$country['countryCode']] = '#fff';
            }
        }

        return view('home.index', [
          'title' => trans('front.title_home'),
          'groups' => $groups,
          'countries' => $countries,
          'scopes' => $scopes,
          'all_countries' => $all_countries,
        ]);
    }

    public function getScope(Request $request, $scope)
    {
        if (!$request->get('group')) {
            $scope = Scope::where('slug', $scope)->get();
            if (!sizeof($scope)) {
                return abort(404);
            }
            $scopes = Scope::all();
            $groups = Group::where('parent', 0)->limit(12)->get();

            return view('home.scope', [
              'title' => trans('front.title_scope'),
              'groups' => $groups,
              'scope' => $scope[0],
              'scopes' => $scopes,
            ]);
        }

        $scope = Scope::where('slug', $scope)->get();
        if (!sizeof($scope)) {
            return abort(404);
        }
        $scopes = Scope::all();
        $groups = Group::where('parent', 0)->limit(12)->get();

        return view('home.scope', [
            'title' => trans('front.title_scope'),
            'groups' => $groups,
            'scope' => $scope[0],
            'scopes' => $scopes,
        ]);
    }

    public function getGroup($group)
    {
        $group = Group::where('slug', $group)->get();

        if (!sizeof($group)) {
            return abort(404);
        }

        $countryCode = B2b::location();

        $country = Country::where('countryCode', $countryCode['countryCode'])->get();
        if (!sizeof($country)) {
            $scope = 1;
        } else {
            if ($country[0]->scope > 0) {
                $scope = $country[0]->scope;
            } else {
                $scope = 1;
            }
        }

        $groups = Group::where('parent', 0)->get();
        $companies = Company::where('status', 'active')->where('group', $group[0]->id)->whereHas('scopes', function ($query) use ($scope) {
          $query->where('scopes.id', $scope);
        })->limit('10')->get();

        return view('home.group', [
          'title' => trans('front.title_group'),
          'companies' => $companies,
          'group' => $group[0],
          'groups' => $groups,
        ]);
    }

    public function getCompany($company)
    {
        $company = Company::where('slug', $company)->where('status', 'active')->get();

        if (!sizeof($company)) {
            return abort(404);
        }

        return view('home.company', [
          'title' => trans('front.title_company'),
          'company' => $company[0],
        ]);
    }

    public function getProduct($product)
    {
        $product = Product::where('id', $product)->where('status', 'publish')->get();

        if (!sizeof($product)) {
            return abort(404);
        }

        return view('home.product', [
        'title' => trans('front.title_product'),
        'product' => $product[0],
      ]);
    }

    public function getLanguage(Request $request)
    {
        $language = Language::where('code', $request->get('lang'))->get();
        if (!sizeof($language)) {
            return redirect('/');
        }
        $language = $language[0];

        $guard = '';
        foreach (['admin', 'company', 'user'] as $gu) {
            if (Auth::guard($gu)->user()) {
                $guard = $gu;
                switch ($gu) {
                    case 'user':
                            $user = Auth::guard($gu)->user();
                            $user->lang_status = 'manual';
                            $user->lang_id = $language->id;
                            $user->save();
                            App::setlocale($language->code);
                        break;

                }
            }
        }

        if (!$gu) {
            Session::put('lang', $language->code);
            App::setlocale($language);
        }
    }

    public function postSearch()
    {
    }

    public function getGcheck($countryCode)
    {
        $country = Country::where('countryCode', $countryCode)->first();
        if (!$country) {
            return redirect('/');
        }

        if ($country->scope > 0) {
            $scope = Scope::find($country->scope);

            return redirect('/scope/'.$scope->slug);
        } else {
            return redirect('/scope/asiaـocenia');
        }
    }
}
