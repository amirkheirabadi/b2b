<?php

namespace App;

use Flash;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Model;
use Mail;

class Company extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;
    protected $table = 'companies';
    protected $primaryKey = 'id';

// Relations
public function address()
{
    return $this->hasMany('App\Address', 'company');
}

    public function group()
    {
        return $this->belongsTo('App\Group', 'company');
    }

    public function scopes()
    {
        return $this->belongsToMany('App\Scope', 'scope_company', 'company', 'scope');
    }

    public function products()
    {
        return $this->hasMany('App\Product', 'company_id');
    }

    // Functions

    public function register()
    {
        $this->confirmation_code = str_random(5);
        $this->status = 'pending';
        $this->save();

        $data = [
          'name' => $this->name,
          'email' => $this->email,
        ];

        Mail::send('emails.company_register', ['name' => $this->name, 'email' => $this->email, 'confirmation_code' => $this->confirmation_code], function ($m) use ($data) {
            $m->to($data['email'], $data['name'])->subject(trans('auth.email_confirmation'));
        });
        Flash::success(trans('auth.register_success'));

        return true;
    }

    public function confirmation($activation_code)
    {
        if ($this->status != 'pending') {
            Flash::success(trans('auth.already_confirm'));

            return true;
        }
        if ($this->confirmation_code != $activation_code) {
            Flash::error(trans('auth.confirmation_wrong'));

            return false;
        }

        $this->status = 'confirmed';
        $this->save();

        Flash::success(trans('auth.confirmation_success'));

        return true;
    }

    public function reconfirmation()
    {
        if ($this->status != 'pending') {
            Flash::success(trans('auth.already_confirm'));

            return true;
        }

        $this->confirmation_code = str_random(5);
        $this->save();

        Mail::send('emails.company_register', ['name' => $this->name, 'email' => $this->email, 'confirmation_code' => $this->confirmation_code], function ($m) use ($data) {
                        $m->to($data['email'], $data['name'])->subject(trans('auth.email_confirmation'));
                    });

        return true;
    }

    public function reset()
    {
        if ($this->status == 'pending') {
            Flash::error(trans('auth.no_confirm'));

            return false;
        }

        $this->reminder_code = str_random(5);
        $this->save();

        Mail::send('emails.reset', ['name' => $this->name, 'email' => $this->email, 'reminder_code' => $this->reminder_code], function ($m) use ($data) {
                $m->to($data['email'], $data['name'])->subject(trans('auth.email_reminder'));
            });
        Flash::success(trans('auth.company_reminder_success'));

        return true;
    }
}
