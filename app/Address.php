<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $table = 'addresses';
    protected $primaryKey = 'id';

    public function company()
    {
      return $this->belongsTo('App\Company','company');
    }
}
