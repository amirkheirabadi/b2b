<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Scopelang extends Model
{
    protected $table = 'scopes_translate';
    protected $primaryKey = 'id';
}
