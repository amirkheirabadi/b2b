<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Scope extends Model
{
    protected $table = 'scopes';
    protected $primaryKey = 'id';

    public function trans()
    {
        $langCode = Language::where('code', \App::getLocale())->first();
        $lang = Scopelang::where('lang_id', $langCode->id)->where('scope_id', $this->id)->get();
        if (sizeof($lang)) {
            $lang = $lang[0];
            $this->name = $lang['name'];
        }

        return $this;
    }
}
