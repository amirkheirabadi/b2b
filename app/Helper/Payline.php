<?php

namespace App\Helper;
use App\Transaction;

class Payline {

	private $api;
	private $redirect;
	private $url;
	private $trans;

	public function __construct($id) {
		$this->api = config("b2b.payline_api");
		$this->redirect = config('b2b.payline_redirect');

		if (config('b2b.payline_test')) {
				$this->url = 'payment-test';
		} else {
					$this->url = 'payment';
		}

		$trans = Transaction::where('id', $id)->where('status', 'pending')->get();
		if (!sizeof($trans)) {
			return [
				'status' => 'error',
				'message' => 'تراکنشی با این مشصخات در سیستم ثبت نشده است .',
			];
		}
		$this->trans = $trans[0];
	}

	public function pay() {
		$body = array("api" => $this->api, "redirect" => $this->redirect, "amount" => $this->trans->price);

		$response = \Unirest\Request::post("http://payline.ir/" . $this->url . "/gateway-send", [], $body);

		if ($response->raw_body < 0) {
			print_r($response->raw_body);
			exit;
		}

		$this->trans->refnum = $response->raw_body;
		$this->trans->save();

		return 'http://payline.ir/payment/gateway-' . $response->raw_body;
	}

	public function check($trans_id) {
		$body = array("api" => $this->api, "id_get" => $this->trans->refnum, "trans_id" => $trans_id);

		$response = Unirest\Request::post("http://payline.ir/" . $this->url . "/gateway-send", [], $body);

		if ($response->raw_body != 1) {
			return false;
		}

		$this->trans->resnum = $trans_id;
		$this->trans->status = 'success';
		$this->trans->save();
		return true;
	}
}
?>
