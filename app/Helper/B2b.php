<?php

namespace App\Helper;

use Flash;
use Validator;
use Gravatar;
use Auth;

class B2b
{
    public static function VldFails($rules, $inputs)
    {
        $validator = Validator::make($inputs, $rules);

        return $validator->fails();
    }

    public static function Vld($rules, $inputs, $url = '')
    {
        $messages = [];

        $validator = Validator::make($inputs, $rules);
        if ($validator->fails()) {
            foreach ($validator->messages()->all() as $error) {
                Flash::error($error);
            }

            if ($url == '') {
                return $messages;
            }

            return redirect($url);
        }
    }

    public static function parseModelValidation($data)
    {
        $data = $data->toArray();
        foreach ($data as $columnName => $errors) {
            foreach ($errors as $error) {
                Flash::error($columnName, $error);
            }
        }
    }

    public static function gravatar($email)
    {
        // if (Gravatar::exists($email)) {
        //     return Gravatar::get($email);
        // } else {
            return '/dashboard_assets/img/avatar.png';
        // }
    }

    public static function user($guard)
    {
        return Auth::guard($guard)->user();
    }

    public static function location()
    {
        $db = new Ip2location(public_path().'/IP2LOCATION-LITE-DB3.BIN', Ip2location::FILE_IO);

        return $db->lookup($_SERVER['REMOTE_ADDR'], Ip2location::ALL);
    }

    public static function countries()
    {
        $country = \App\Country::all();

        return $country;
    }

    public static function langs()
    {
        $lang = \App\Language::all();

        return $lang;
    }
}
