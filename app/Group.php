<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $table = 'groups';
    protected $primaryKey = 'id';

    public function companies()
    {
        return $this->belongsToMany('App\Company', 'group');
    }

    public function trans()
    {
        $langCode = Language::where('code', \App::getLocale())->first();
        $lang = Grouplang::where('lang_id', $langCode->id)->where('group_id', $this->id)->get();
        if (sizeof($lang)) {
            $lang = $lang[0];
            $this->name = $lang['name'];
            $this->description = $lang['description'];
        }

        return $this;
    }

    public function child()
    {
        return self::where('parent', $this->id)->get();
    }
}
