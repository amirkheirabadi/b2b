<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $table = 'transactions';
    protected $primaryKey = 'id';

    public function company()
    {
        return $this->belongsTo('App\Company', 'owner', 'id');
    }
}
