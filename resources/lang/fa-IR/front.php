<?php

return [
  'title_home' => 'صفحه اصلی',
  'title_scope' => 'نمایش منطقه',
  'title_group' => 'نمایش گروه',
  'title_company' => 'مشاهده اطلاعات شرکت',
  'title_product' => 'نمایش محصول',

  'menu_myaccount' => 'حساب کاربری',
  'menu_login' => 'ورود به حساب کاربری',
  'menu_register_as_user' => 'ثبت حساب کاربری جدید',
  'menu_register_as_company' => 'ثبت شرکت جدید',

  'nav_home' => 'صفحه اصلی',
  'nav_faq' => 'سوالات متداول',
  'nav_contact' => 'ارتباط با ما',
  'nav_about' => 'درباره ما',

  'search_pholder' => 'جستجو در سایت',

  'menu_button_minimize' => 'نمایش منو',

  'footer_address_title' => 'آدرس ما :',
  'footer_address' => 'ایران - مشهد',
  'footer_phone_title' => 'تلفن تماس :',
  'footer_phone' => '05123123',
  'footer_email_title' => 'آدرس ایمیل :',
  'footer_email' => 'ُinfo@demo.com',

  'show_information' => 'نمایش اطلاعات',

  'menu_signin' => 'ورود',
  'menu_signup' => 'ثبت نام',
];
