<?php

return [

	// titles
	'title_signin' => 'ورد به سیستم',
	'title_singup' => 'ثیت نام در سیستم',
	'title_reconfirmation' => 'ارسال مجدد کد فعال سازی',
	'title_reminder' => 'فراموشی رمز عبور',
	'title_change_password' => 'تغییر رمز عبور',
	'title_signup_company' => 'ثبت شرکت',
	'title_signup_company_success' => 'موفقیت ثبت نام شرکت',
	// Message
	'user_not_found' => 'کاربری با این مشصخات در سیستم ثیت نشده است .',
	'comapny_not_found' => 'شرکت یافت نشد',
	'register_success' => 'برای تایید حساب کاربری لطفا بروی لینکی که به ایمیل شما ارسال شده است کلیک کنید .',
	'already_confirm' => 'حساب کاربری شما قبلا تایید شده است .',
	'no_confirm' => 'حساب کاربری شما هنوز تایید نشده است .',
	'confirmation_wrong' => 'کد فعال سازی شما اشتباه می باشد .',
	'confirmation_success' => 'حساب کاربری شما با موفقیت تایید شد حالا می توایند وارد سایت شوید',
	'reminder_success' => 'ایمیل حاوی لینی تغویض رمز عبور ارسال شد .',
	'reset_code_wrong' => 'کد تغییر رمز عبور اشتباه است',
	'email_password_wrong' => 'نام کاربری و یا رمز عبور اشتباه می باشد ',
	'success_password_change' => 'رمز عبور با موفقیت تغییر کرد .',
	// Email title
	'email_reminder' => '',
	'email_confirmation' => '',

];

?>
