@extends('layouts.admin')

@section('body')
<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>ایجاد کاربر جدید</h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">

<div class="row">
  <div class="col-md-12 ">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet light">
						<div class="portlet-body form">
							<form role="form" method="post" action="{{URL::to('/dashboard/users/create')}}">
								<div class="form-body">

                  <div class="form-group">
                    <div class="col-md-6">
                      <label>نام</label>
                      <div class="input-icon">
                        <i class="fa fa-user"></i>
                        <input type="text" class="form-control" name="first_name">
                      </div>
                    </div>

                    <div class="col-md-6">
                      <label>نام خانوادگی</label>
                      <div class="input-icon">
                        <i class="fa fa-user"></i>
                        <input type="text" class="form-control" name="last_name">
                      </div>
                    </div>
                  </div>

&nbsp<br><br>
                  <div class="form-group">
                    <div class="col-md-6">
                      <label>ایمیل</label>
                      <div class="input-icon">
                        <i class="fa fa-send"></i>
                        <input type="text" class="form-control" name="email">
                      </div>
                    </div>

                    <div class="col-md-6">
                      <label>موبایل</label>
                      <div class="input-icon">
                        <i class="fa fa-mobile-phone"></i>
                        <input type="text" class="form-control" name="mobile">
                      </div>
                    </div>
                  </div>


&nbsp<br><br>
                  <div class="form-group">
  										<label>وضعیت</label>
  										<select class="form-control" name="status">
  											<option value="active">فعال</option>
  											<option value="pending">در انتظار فعال شدن</option>
  											<option value="suspend">مسدود سازی حساب</option>
  										</select>
  									</div>
&nbsp<br><br>
                  <div class="form-group">
                    <div class="col-md-6">
                      <label>رمز عبور</label>
                      <div class="input-icon">
                        <i class="fa fa-key"></i>
                        <input type="text" class="form-control" name="password">
                      </div>
                    </div>

                    <div class="col-md-6">
                      <label>تکرار رمز عبور</label>
                      <div class="input-icon">
                        <i class="fa fa-key"></i>
                        <input type="text" class="form-control" name="password_confirmation">
                      </div>
                    </div>
                  </div>
&nbsp<br><br>
								</div>
								<div class="form-actions">
									<a type="submit" class="btn blue">ثبت کاربر</a>
									<a href="{{URL::to('/dashboard/users')}}" class="btn default">انصراف</a>
								</div>
							</form>
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
				</div>
</div>
@stop
