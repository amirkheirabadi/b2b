@extends('layouts.front')

@section('header')
  <link rel="stylesheet" href="/assets/css/jquery-jvectormap-2.0.3.css" media="screen" title="no title" charset="utf-8">
  <link rel="stylesheet" type="text/css" href="assets/css/option5.css" />
@endsection

@section('body')
  <div class="map_main">
    <div class="container">
        <div class="row">
          <div class="col-md-4">
            <ul id="map_helper">
              @foreach ($scopes as $scope)
                <?php $scope->trans(); ?>
                <li>
                  <i class="fa fa-map-marker"></i></span><h4 id="{{$scope->slug}}">{{$scope->name}}</h4>
                </li>
              @endforeach
            </ul>
          </div>
          <div class="col-md-8">
            <div id="world-map" style="width: 800px; height: 550px"></div>
          </div>
        </div>
    </div>
  </div>
  <div id="content-wrap main_group_list">
      <div class="container">
          <!-- service 2 -->
          <div class="services">
              <ul>
                <div class="row">
                <?php $index = 0; ?>
                @foreach ($groups as $group)

                  <?php $group->trans();
                  ?>
                  <div class="col-xs-3 col-sm-6 col-md-3 services-item">
                      <div class="service-wapper">
                                  <div class="icon">
                                      <a href="{{URL::to('/group/'.$group->slug)}}">
                                        <img width="150" src="{{$group->thumb}}" >
                                      </a>
                                  </div>
                                  <h3 class="title"><a href="{{URL::to('/group/'.$group->slug)}}">{{$group->name}} </a></h3>
                      </div>
                  </div>
                  <?php
                  if (in_array($index,[3,7,11]) ) {
                    echo "</div><br><br><br><div class='row'>";
                  }
                ?>
                  <?php
                  $index ++;?>
                @endforeach
                  </div>
              </ul>
          </div>
          <!-- ./service 2 -->
      </div> <!-- /.container -->
  </div>
  <br>
@endsection


@section('footer')

    <script type="text/javascript" src="/assets/js/jquery-jvectormap-2.0.3.min.js"></script>
    <script type="text/javascript" src="/assets/js/jquery-jvectormap-world-mill.js"></script>

        <script>
        var selectScope = '';
         $(function(){
           var countries =  <?php echo json_encode($countries) ?>;
            var all_countries =  <?php echo json_encode($all_countries) ?>;
           var amar ={"CN":9240484,"IQ":6140301,'AE':3673543,"AF": 2386302,"IN":2153890,"TR":1919132,"AD":157,"AL":1.325,"AM":117.569,"AR":7.536,"AT":9.309,"AU":22.054,"AZ":442.356,"BA":965,"BD":65.153,"BE":27.752,"BG":29.561,"BH":10.104,"BR":45.447,"BY":5.698,"CA":14.864,"CH":9.715,"CL":129,"CO":1.518,"CR":0,"CU":1.027,"CY":2.018,"CZ":4.193,"DE":358.99,"DJ":10.031,"DK":11.903,"DO":0,"DZ":5.09,"EC":61,"EE":42,"EG":595.356,"ES":174.853,"FI":298,"FJ":0,"FR":26.398,"GB":103.059,"GE":110.763,"GR":6.18,"GT":73,"GY":0,"HN":0,"HR":535,"HT":23,"HU":2.841,"ID":61.502,"IE":367,"IS":0,"IT":466.122,"JM":30,"JO":26.354,"JP":32.615,"KG":39.46,"KH":1.779,"KM":22,"KW":160.681,"KZ":230.672,"LB":74.646,"LK":122.433,"LT":4.5,"LU":189,"LV":90,"MA":1.471,"ME":1.618,"MN":0,"MR":89,"MT":0,"MV":377,"MX":1.84,"MY":63.369,"NI":0,"NL":81.138,"NO":1.203,"NP":470,"NZ":1.649,"OM":292.973,"PA":219,"PE":65,"PH":59.552,"PK":837.113,"PL":20.653,"PT":18.234,"PY":171,"QA":82.567,"RO":12.493,"RS":2.005,"SA":136.626,"SE":14.666,"SG":8.501,"SI":702,"SK":5.178,"SO":11.016,"SR":486,"SV":0,"TH":112.737,"TJ":224.35,"TM":944.467,"TN":17.821,"TT":0,"UA":33.396,"UY":8.603,"UZ":106.301,"YE":21.562};
         var map=new jvm.Map({
            map: 'world_mill',
            container:$('#world-map'),
            zoomOnScroll:0,
            panOnDrag:0,
            zoomOnScrollSpeed:0,
            zoomMax:1,
            regionStyle:{
            initial: {
        fill: '#fff',
        "stroke-width": 0,
        stroke: 'none',
      }},
            // colors:countries,
            series: {
           regions: [{

               attribute: 'fill',

           }]
       },
       onRegionTipShow: function(event, label, code) {
        //  if (amar.hasOwnProperty(code) && amar[code] != 0) {
        //    label.html(label.html()+'    ('++' $  )');
        //  }

        if (selectScope !='' && countries[selectScope].hasOwnProperty(code)) {
          sum = 0 ;
          for (k in countries[selectScope]) {
            if (amar[k] != undefined) {
              sum = sum+amar[k];
            }
          }
          label.html(sum + ' $');
        }else{
          return false;
        }
       },
         onRegionClick:function (e,code) {
           location.assign('{{URL::to('/gcheck')}}/'+code);
         }
       });


       $('#map_helper h4').click(function () {
         var location = $(this).attr('id');

         map.series.regions[0].setValues(all_countries);
         selectScope = location;

         console.log(countries[location]);
          map.series.regions[0].setValues(countries[location]);
       });
      });


   </script>
@endsection
