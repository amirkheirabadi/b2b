@extends('layouts.front')

@section('header')
  <link rel="stylesheet" type="text/css" href="/assets/lib/jquery.bxslider/jquery.bxslider.css" />
  <link rel="stylesheet" type="text/css" href="/assets/lib/owl.carousel/owl.carousel.css" />
  <link rel="stylesheet" type="text/css" href="/assets/lib/jquery-ui/jquery-ui.css" />
@endsection

@section('body')

  <div class="columns-container">
      <div class="container" id="columns">
          <!-- row -->
          <div class="row">
              <!-- Left colunm -->
              <div class="column col-xs-12 col-sm-3" id="left_column">
                  <!-- block category -->
                  <div class="block left-module">
                    <?php $group->trans() ?>
                      <p class="title_block">{{$group->name}}</p>
                      <div class="block_content">
                          <!-- layered -->
                          <div class="layered">
                              <div class="layered-content">
                                  <div class="today-deals">
                                    {{$group->description}}
                                  </div>
                              </div>
                          </div>
                          <!-- ./layered -->
                      </div>
                  </div>
                  <!-- ./block category  -->


                  <!-- SPECIAL -->
                  <div class="block left-module">
                      <p class="title_block">برخی محصولات</p>
                      <div class="block_content">
                          <ul class="products-block">
                              <li>
                                  <div class="products-block-left">
                                      <a href="#">
                                          <img src="/assets/data/product-100x122.jpg" alt="SPECIAL PRODUCTS">
                                      </a>
                                  </div>
                                  <div class="products-block-right">
                                      <p class="product-name">
                                          <a href="#">Woman Within Plus Size Flared</a>
                                      </p>
                                      <p class="product-price">$38,95</p>
                                      <p class="product-star">
                                          <i class="fa fa-star"></i>
                                          <i class="fa fa-star"></i>
                                          <i class="fa fa-star"></i>
                                          <i class="fa fa-star"></i>
                                          <i class="fa fa-star-half-o"></i>
                                      </p>
                                  </div>
                              </li>
                          </ul>
                          <div class="products-block">
                              <div class="products-block-bottom">
                                  <a class="link-all" href="#">All Products</a>
                              </div>
                          </div>
                      </div>
                  </div>
                  <!-- ./SPECIAL -->
              </div>
              <!-- ./left colunm -->
              <!-- Center colunm-->
              <div class="center_column col-xs-12 col-sm-9" id="center_column">
                  <!-- category-slider -->
                  <div class="category-slider">
                      <ul class="owl-carousel owl-style2" data-dots="false" data-loop="true" data-nav = "true" data-autoplayTimeout="1000" data-autoplayHoverPause = "true" data-items="1">
                          <li>
                              <img src="/assets/data/category-slide.jpg" alt="category-slider">
                          </li>
                          <li>
                              <img src="/assets/data/slide-cart2.jpg" alt="category-slider">
                          </li>
                      </ul>
                  </div>
                  <!-- ./category-slider -->

                  <!-- view-product-list-->
                  <div id="view-product-list" class="view-product-list">
                      <!-- PRODUCT LIST -->
                      <ul class="row product-list style2 grid">
                        @foreach ($companies as $company)
                          <li class="col-sx-12 col-sm-4">
                              <div class="product-container">
                                  <div class="left-block">
                                      <a href="#">
                                          <img class="img-responsive" alt="product" src="{{$company->logo}}" />
                                      </a>
                                      <div class="quick-view">
                                              <a title="Add to my wishlist" class="heart" href="#"></a>
                                              <a title="Add to compare" class="compare" href="#"></a>
                                              <a title="Quick view" class="search" href="#"></a>
                                      </div>
                                  </div>
                                  <div class="right-block">
                                      <h5 class="product-name"><a href="{{URL::to('/company/'.$company->slug)}}">{{$company->name}}</a></h5>

                                      <div class="content_price">
                                          مدیر عامل : {{$company->owner}}
                                      </div>
                                      <div class="add-to-cart">
                                          <a href="{{URL::to('/company/'.$company->slug)}}">{{trans('front.show_information')}}</a>
                                      </div>
                                  </div>
                              </div>
                          </li>
                          @endforeach
                      </ul>
                      <!-- ./PRODUCT LIST -->
                  </div>
                  <!-- ./view-product-list-->
                  {{-- <div class="sortPagiBar">
                      <div class="bottom-pagination">
                          <nav>
                            <ul class="pagination">
                              <li class="active"><a href="#">1</a></li>
                              <li><a href="#">2</a></li>
                              <li><a href="#">3</a></li>
                              <li><a href="#">4</a></li>
                              <li><a href="#">5</a></li>
                              <li>
                                <a href="#" aria-label="Next">
                                  <span aria-hidden="true">Next &raquo;</span>
                                </a>
                              </li>
                            </ul>
                          </nav>
                      </div>
                      <div class="show-product-item">
                          <select name="">
                              <option value="">Show 18</option>
                              <option value="">Show 20</option>
                              <option value="">Show 50</option>
                              <option value="">Show 100</option>
                          </select>
                      </div>
                      <div class="sort-product">
                          <select>
                              <option value="">Product Name</option>
                              <option value="">Price</option>
                          </select>
                          <div class="sort-product-icon">
                              <i class="fa fa-sort-alpha-asc"></i>
                          </div>
                      </div>
                  </div> --}}
              </div>
              <!-- ./ Center colunm -->
          </div>
          <!-- ./row-->
      </div>
  </div>
@endsection

@section('footer')
  <script type="text/javascript" src="/assets/lib/jquery.bxslider/jquery.bxslider.min.js"></script>
  <script type="text/javascript" src="/assets/lib/owl.carousel/owl.carousel.min.js"></script>
  <script>
  var slider = $('#contenhomeslider').bxSlider(
      {
          nextText:'<i class="fa fa-angle-right"></i>',
          prevText:'<i class="fa fa-angle-left"></i>',
          auto: true,
      }

  );

  </script>
@endsection
