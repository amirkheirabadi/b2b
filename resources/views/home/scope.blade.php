@extends('layouts.front')

@section('header')
  <link rel="stylesheet" type="text/css" href="/assets/lib/jquery.bxslider/jquery.bxslider.css" />
  <link rel="stylesheet" type="text/css" href="/assets/lib/owl.carousel/owl.carousel.css" />
  <link rel="stylesheet" type="text/css" href="/assets/lib/jquery-ui/jquery-ui.css" />

@endsection

@section('body')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <ul id="scope_list">
        @foreach ($scopes as $sc)
          <?php $sc->trans(); ?>
          @if ($sc->id == $scope['id'])
            <li id="current_scope"><a href="{{URL::to('/scope/'.$sc->slug)}}"  style="color:#ff3366">{{$sc->name}}</a></li>
          @else
            <li ><a href="{{URL::to('/scope/'.$sc->slug)}}">{{$sc->name}}</a></li>
          @endif
        @endforeach
      </ul>
    </div>
    <div class="col-md-3">
        <div class="box-vertical-megamenus">
        <div class="vertical-menu-content is-home">
            <ul class="vertical-menu-list">
                @foreach ($groups as $group)
                  <?php $group->trans(); ?>
                    <li>
                        <a href="{{URL::to('/scope/'.$scope->slug.'/'.$group->slug)}}"><img class="icon-menu" alt="Funky roots" src="{{$group->thumb}}">{{$group->name}}</a>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
    </div>

    <div class="col-md-9 header-top-right">
        <div class="homeslider">
            <div class="content-slide">
                <ul id="contenhomeslider">
                  <li><img alt="Funky roots" src="/assets/data/slide.jpg" title="Funky roots" /></li>
                  <li><img alt="Funky roots" src="/assets/data/slide.jpg" title="Funky roots" /></li>
                  <li><img  alt="Funky roots" src="/assets/data/slide.jpg" title="Funky roots" /></li>
                </ul>
            </div>
        </div>
        <div class="header-banner banner-opacity">
            <a href="#"><img alt="Funky roots" src="/assets/data/ads1.jpg" /></a>
        </div>
    </div>

  </div>
</div>

@foreach ($groups as $group)
  <?php $group->trans() ?>
<div class="content-page" style="margin:0">
  <div class="container">
    <!-- featured category fashion -->
    <div class="category-featured fashion">
        <nav class="navbar nav-menu show-brand">
          <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
              <div class="navbar-brand"><a href="{{URL::to('/scope/'.$scope->slug.'/'.$group->slug)}}"><img alt="fashion" src="{{$group->thumb}}" />{{$group->name}}</a></div>
              <span class="toggle-menu"></span>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse">
              <ul class="nav navbar-nav">
                <li><a data-toggle="tab" href="#tab-{{$group->id}}_company">شرکت ها</a></li>
                <li><a data-toggle="tab" href="#tab-{{$group->id}}_product">محصولات</a></li>
              </ul>
            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>
       <div class="product-featured clearfix">
            <div class="row">
                <div class="col-sm-3 sub-category-wapper">
                    <ul class="sub-category-list">
                      <?php $childs =$group->child(); ?>
                      @foreach ($childs as $child)
                        <?php $child->trans() ?>
                        <li><a href="{{URL::to('/scope/'.$scope->slug.'/'.$child->slug)}}">{{$child->name}}</a></li>
                      @endforeach
                    </ul>
                </div>
                <div class="col-sm-9 col-right-tab">
                    <div class="product-featured-tab-content">
                        <div class="tab-container">
                            <div class="tab-panel active" id="#tab-{{$group->id}}_company">
                                    <ul class="product-list row">
                                      <?php
                                          $list = [$group->id];
                                          foreach ($childs as $ch) {
                                            array_push($list,$ch->id);
                                          }
                                          $companies = App\Company::whereIn('group',$list)->limit(6)->get();
                                       ?>
                                       @foreach ($companies as $company)
                                        <li class="col-sm-4">
                                            <div class="right-block">
                                                <h5 class="product-name"><a href="{{URL::to('/company/'.$company->slug)}}">{{$company->name}}</a></h5>
                                            </div>
                                            <div class="left-block">
                                                <a href="#"><img class="img-responsive" alt="product" src="{{$company->logo}}" /></a>
                                                <div class="add-to-cart">
                                                    <a title="Add to Cart" href="{{URL::to('/company/'.$company->slug)}}">{{trans('front.show_information')}}</a>
                                                </div>
                                            </div>
                                            <br>
                                            نام شرکت : {{$company->name}}<br>
                                            مدیرعامل : {{$company->owner}}
                                            <br>
                                        </li>
                                        @endforeach
                                   </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
       </div>
    </div>
  </div>
</div>

@endforeach

@endsection

@section('footer')
  <script type="text/javascript" src="/assets/lib/jquery.bxslider/jquery.bxslider.min.js"></script>
  <script type="text/javascript" src="/assets/lib/owl.carousel/owl.carousel.min.js"></script>
  <script>
  var slider = $('#contenhomeslider').bxSlider(
      {
          nextText:'<i class="fa fa-angle-right"></i>',
          prevText:'<i class="fa fa-angle-left"></i>',
          auto: true,
      }

  );

  </script>
@endsection
