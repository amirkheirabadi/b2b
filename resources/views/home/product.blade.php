@extends('layouts.front')

@section('header')
  <link rel="stylesheet" type="text/css" href="/assets/lib/jquery.bxslider/jquery.bxslider.css" />
  <link rel="stylesheet" type="text/css" href="/assets/lib/owl.carousel/owl.carousel.css" />
  <link rel="stylesheet" type="text/css" href="/assets/lib/jquery-ui/jquery-ui.css" />
  <link rel="stylesheet" type="text/css" href="/assets/lib/fancyBox/jquery.fancybox.css" />
@endsection

@section('body')
  <div class="columns-container">
      <div class="container" id="columns">
          <!-- row -->
          <div class="row">
              <!-- Left colunm -->
              <div class="column col-xs-12 col-sm-3" id="left_column">
                  <!-- block category -->
                  <div class="block left-module">
                      <p class="title_block">محصولات دیگر</p>
                      <div class="block_content">
                          <!-- layered -->
                          <div class="layered layered-category">
                              <div class="layered-content">

                              </div>
                          </div>
                          <!-- ./layered -->
                      </div>
                  </div>
                  <!-- ./block category  -->
                  <!-- block best sellers -->
                  <div class="block left-module">
                      <p class="title_block">شرکت های مشابه</p>
                      <div class="block_content">

                      </div>
                  </div>
                  <!-- ./block best sellers  -->
              </div>
              <!-- ./left colunm -->
              <!-- Center colunm-->
              <div class="center_column col-xs-12 col-sm-9" id="center_column">
                  <!-- Product -->
                      <div id="product">
                          <div class="primary-box row">
                              <div class="pb-left-column col-xs-12 col-sm-6">
                                  <!-- product-imge-->
                                  <div class="product-image">
                                      <div class="product-full">
                                          <img id="product-zoom" src='{{URL::to($product->thumb)}}' />
                                      </div>
                                      {{-- <div class="product-img-thumb" id="gallery_01">
                                          <ul class="owl-carousel" data-items="3" data-nav="true" data-dots="false" data-margin="20" data-loop="true">
                                              <li>
                                                  <a href="#" data-image="/assets/data/product-s3-420x512.jpg" data-zoom-image="/assets/data/product-s3-850x1036.jpg">
                                                      <img id="product-zoom"  src="/assets/data/product-s3-100x122.jpg" />
                                                  </a>
                                              </li>
                                              <li>
                                                  <a href="#" data-image="/assets/data/product-s2-420x512.jpg" data-zoom-image="/assets/data/product-s2-850x1036.jpg">
                                                      <img id="product-zoom"  src="/assets/data/product-s2-100x122.jpg" />
                                                  </a>
                                              </li>
                                              <li>
                                                  <a href="#" data-image="/assets/data/product-420x512.jpg" data-zoom-image="/assets/data/product-850x1036.jpg">
                                                      <img id="product-zoom"  src="/assets/data/product-100x122.jpg" />
                                                  </a>
                                              </li>
                                              <li>
                                                  <a href="#" data-image="/assets/data/product-s4-420x512.jpg" data-zoom-image="/assets/data/product-s4-850x1036.jpg">
                                                      <img id="product-zoom"  src="/assets/data/product-s4-100x122.jpg" />
                                                  </a>
                                              </li>
                                              <li>
                                                  <a href="#" data-image="/assets/data/product-s5-420x512.jpg" data-zoom-image="/assets/data/product-s5-850x1036.jpg">
                                                      <img id="product-zoom"  src="/assets/data/product-s5-100x122.jpg" />
                                                  </a>
                                              </li>
                                              <li>
                                                  <a href="#" data-image="/assets/data/product-s6-420x512.jpg" data-zoom-image="/assets/data/product-s6-850x1036.jpg">
                                                      <img id="product-zoom"  src="/assets/data/product-s6-100x122.jpg" />
                                                  </a>
                                              </li>
                                          </ul>
                                      </div> --}}
                                  </div>
                                  <!-- product-imge-->
                              </div>
                              <div class="pb-right-column col-xs-12 col-sm-6" style="direction:rtl">
                                <br>
                                  <h1 class="product-name">{{$product->name}}</h1>
<br>
                                  <div class="product-price-group">
                                      <span class="price">  ریال{{$product->price}}</span>
                                  </div>
                                  <div class="info-orther">
                                    <p>
                                      مدل : {{$product->model}}
                                    </p>
                                    <p>
                                      بسسته بندی : {{$product->packing}}
                                    </p>
                                    <p>
                                      قدرت تولید : {{$product->power_size}}
                                    </p>
                                  </div>
                                  <div class="product-desc">
                                  {{$product->description}}
                                  </div>


                                  <div class="form-action">
                                      <div class="button-group">
                                          <a class="btn-add-cart" href="#">سفارش</a>
                                      </div>
                                      <div class="button-group">
                                          <a class="wishlist" href="#"><i class="fa fa-heart-o"></i>
                                          <br>Wishlist</a>
                                          <a class="compare" href="#"><i class="fa fa-signal"></i>
                                          <br>
                                          Compare</a>
                                      </div>
                                  </div>
                                  <div class="form-share">
                                      <div class="sendtofriend-print">
                                          <a href="javascript:print();"><i class="fa fa-print"></i> Print</a>
                                          <a href="#"><i class="fa fa-envelope-o fa-fw"></i>Send to a friend</a>
                                      </div>
                                      <div class="network-share">
                                      </div>
                                  </div>
                              </div>
                          </div>
                          <!-- tab product -->
                          {{-- <div class="product-tab">
                              <ul class="nav-tab">
                                  <li class="active">
                                      <a aria-expanded="false" data-toggle="tab" href="#product-detail">Product Details</a>
                                  </li>
                                  <li>
                                      <a aria-expanded="true" data-toggle="tab" href="#information">information</a>
                                  </li>
                                  <li>
                                      <a data-toggle="tab" href="#reviews">reviews</a>
                                  </li>
                                  <li>
                                      <a data-toggle="tab" href="#extra-tabs">Extra Tabs</a>
                                  </li>
                                  <li>
                                      <a data-toggle="tab" href="#guarantees">guarantees</a>
                                  </li>
                              </ul>
                              <div class="tab-container">
                                  <div id="product-detail" class="tab-panel active">
                                      <p>Morbi mollis tellus ac sapien. Nunc nec neque. Praesent nec nisl a purus blandit viverra. Nunc nec neque. Pellentesque auctor neque nec urna.</p>

                                      <p>Curabitur suscipit suscipit tellus. Cras id dui. Nam ipsum risus, rutrum vitae, vestibulum eu, molestie vel, lacus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Maecenas vestibulum mollis diam.</p>

                                      <p>Vestibulum facilisis, purus nec pulvinar iaculis, ligula mi congue nunc, vitae euismod ligula urna in dolor. Sed lectus. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Nam at tortor in tellus interdum sagittis. Pellentesque egestas, neque sit amet convallis pulvinar, justo nulla eleifend augue, ac auctor orci leo non est.</p>
                                  </div>
                                  <div id="information" class="tab-panel">
                                      <table class="table table-bordered">
                                          <tr>
                                              <td width="200">Compositions</td>
                                              <td>Cotton</td>
                                          </tr>
                                          <tr>
                                              <td>Styles</td>
                                              <td>Girly</td>
                                          </tr>
                                          <tr>
                                              <td>Properties</td>
                                              <td>Colorful Dress</td>
                                          </tr>
                                      </table>
                                  </div>
                                  <div id="reviews" class="tab-panel">
                                      <div class="product-comments-block-tab">
                                          <div class="comment row">
                                              <div class="col-sm-3 author">
                                                  <div class="grade">
                                                      <span>Grade</span>
                                                      <span class="reviewRating">
                                                          <i class="fa fa-star"></i>
                                                          <i class="fa fa-star"></i>
                                                          <i class="fa fa-star"></i>
                                                          <i class="fa fa-star"></i>
                                                          <i class="fa fa-star"></i>
                                                      </span>
                                                  </div>
                                                  <div class="info-author">
                                                      <span><strong>Jame</strong></span>
                                                      <em>04/08/2015</em>
                                                  </div>
                                              </div>
                                              <div class="col-sm-9 commnet-dettail">
                                                  Phasellus accumsan cursus velit. Pellentesque egestas, neque sit amet convallis pulvinar
                                              </div>
                                          </div>
                                          <div class="comment row">
                                              <div class="col-sm-3 author">
                                                  <div class="grade">
                                                      <span>Grade</span>
                                                      <span class="reviewRating">
                                                          <i class="fa fa-star"></i>
                                                          <i class="fa fa-star"></i>
                                                          <i class="fa fa-star"></i>
                                                          <i class="fa fa-star"></i>
                                                          <i class="fa fa-star"></i>
                                                      </span>
                                                  </div>
                                                  <div class="info-author">
                                                      <span><strong>Author</strong></span>
                                                      <em>04/08/2015</em>
                                                  </div>
                                              </div>
                                              <div class="col-sm-9 commnet-dettail">
                                                  Phasellus accumsan cursus velit. Pellentesque egestas, neque sit amet convallis pulvinar
                                              </div>
                                          </div>
                                          <p>
                                              <a class="btn-comment" href="#">Write your review !</a>
                                          </p>
                                      </div>

                                  </div>
                                  <div id="extra-tabs" class="tab-panel">
                                      <p>Phasellus accumsan cursus velit. Pellentesque egestas, neque sit amet convallis pulvinar, justo nulla eleifend augue, ac auctor orci leo non est. Sed lectus. Sed a libero. Vestibulum eu odio.</p>

                                      <p>Maecenas vestibulum mollis diam. In consectetuer turpis ut velit. Curabitur at lacus ac velit ornare lobortis. Praesent ac sem eget est egestas volutpat. Nam eget dui.</p>

                                      <p>Maecenas nec odio et ante tincidunt tempus. Vestibulum suscipit nulla quis orci. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Aenean ut eros et nisl sagittis vestibulum. Aliquam eu nunc.</p>
                                  </div>
                                  <div id="guarantees" class="tab-panel">
                                      <p>Phasellus accumsan cursus velit. Pellentesque egestas, neque sit amet convallis pulvinar, justo nulla eleifend augue, ac auctor orci leo non est. Sed lectus. Sed a libero. Vestibulum eu odio.</p>

                                      <p>Maecenas vestibulum mollis diam. In consectetuer turpis ut velit. Curabitur at lacus ac velit ornare lobortis. Praesent ac sem eget est egestas volutpat. Nam eget dui.</p>

                                      <p>Maecenas nec odio et ante tincidunt tempus. Vestibulum suscipit nulla quis orci. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Aenean ut eros et nisl sagittis vestibulum. Aliquam eu nunc.</p>
                                      <p>Maecenas vestibulum mollis diam. In consectetuer turpis ut velit. Curabitur at lacus ac velit ornare lobortis. Praesent ac sem eget est egestas volutpat. Nam eget dui.</p>
                                  </div>
                              </div>
                          </div> --}}
                          <!-- ./tab product -->
                          <!-- box product -->
                          {{-- <div class="page-product-box">
                              <h3 class="heading">Related Products</h3>
                              <ul class="product-list owl-carousel" data-dots="false" data-loop="true" data-nav = "true" data-margin = "30" data-autoplayTimeout="1000" data-autoplayHoverPause = "true" data-responsive='{"0":{"items":1},"600":{"items":3},"1000":{"items":3}}'>
                                  <li>
                                      <div class="product-container">
                                          <div class="left-block">
                                              <a href="#">
                                                  <img class="img-responsive" alt="product" src="/assets/data/p40.jpg" />
                                              </a>
                                              <div class="quick-view">
                                                      <a title="Add to my wishlist" class="heart" href="#"></a>
                                                      <a title="Add to compare" class="compare" href="#"></a>
                                                      <a title="Quick view" class="search" href="#"></a>
                                              </div>
                                              <div class="add-to-cart">
                                                  <a title="Add to Cart" href="#add">Add to Cart</a>
                                              </div>
                                          </div>
                                          <div class="right-block">
                                              <h5 class="product-name"><a href="#">Maecenas consequat mauris</a></h5>
                                              <div class="product-star">
                                                  <i class="fa fa-star"></i>
                                                  <i class="fa fa-star"></i>
                                                  <i class="fa fa-star"></i>
                                                  <i class="fa fa-star"></i>
                                                  <i class="fa fa-star-half-o"></i>
                                              </div>
                                              <div class="content_price">
                                                  <span class="price product-price">$38,95</span>
                                                  <span class="price old-price">$52,00</span>
                                              </div>
                                          </div>
                                      </div>
                                  </li>
                                  <li>
                                      <div class="product-container">
                                          <div class="left-block">
                                              <a href="#">
                                                  <img class="img-responsive" alt="product" src="/assets/data/p35.jpg" />
                                              </a>
                                              <div class="quick-view">
                                                      <a title="Add to my wishlist" class="heart" href="#"></a>
                                                      <a title="Add to compare" class="compare" href="#"></a>
                                                      <a title="Quick view" class="search" href="#"></a>
                                              </div>
                                              <div class="add-to-cart">
                                                  <a title="Add to Cart" href="#add">Add to Cart</a>
                                              </div>
                                          </div>
                                          <div class="right-block">
                                              <h5 class="product-name"><a href="#">Maecenas consequat mauris</a></h5>
                                              <div class="product-star">
                                                  <i class="fa fa-star"></i>
                                                  <i class="fa fa-star"></i>
                                                  <i class="fa fa-star"></i>
                                                  <i class="fa fa-star"></i>
                                                  <i class="fa fa-star-half-o"></i>
                                              </div>
                                              <div class="content_price">
                                                  <span class="price product-price">$38,95</span>
                                                  <span class="price old-price">$52,00</span>
                                              </div>
                                          </div>
                                      </div>
                                  </li>
                                  <li>
                                      <div class="product-container">
                                          <div class="left-block">
                                              <a href="#">
                                                  <img class="img-responsive" alt="product" src="/assets/data/p37.jpg" />
                                              </a>
                                              <div class="quick-view">
                                                      <a title="Add to my wishlist" class="heart" href="#"></a>
                                                      <a title="Add to compare" class="compare" href="#"></a>
                                                      <a title="Quick view" class="search" href="#"></a>
                                              </div>
                                              <div class="add-to-cart">
                                                  <a title="Add to Cart" href="#add">Add to Cart</a>
                                              </div>
                                          </div>
                                          <div class="right-block">
                                              <h5 class="product-name"><a href="#">Maecenas consequat mauris</a></h5>
                                              <div class="product-star">
                                                  <i class="fa fa-star"></i>
                                                  <i class="fa fa-star"></i>
                                                  <i class="fa fa-star"></i>
                                                  <i class="fa fa-star"></i>
                                                  <i class="fa fa-star-half-o"></i>
                                              </div>
                                              <div class="content_price">
                                                  <span class="price product-price">$38,95</span>
                                                  <span class="price old-price">$52,00</span>
                                              </div>
                                          </div>
                                      </div>
                                  </li>
                                  <li>
                                      <div class="product-container">
                                          <div class="left-block">
                                              <a href="#">
                                                  <img class="img-responsive" alt="product" src="/assets/data/p39.jpg" />
                                              </a>
                                              <div class="quick-view">
                                                      <a title="Add to my wishlist" class="heart" href="#"></a>
                                                      <a title="Add to compare" class="compare" href="#"></a>
                                                      <a title="Quick view" class="search" href="#"></a>
                                              </div>
                                              <div class="add-to-cart">
                                                  <a title="Add to Cart" href="#add">Add to Cart</a>
                                              </div>
                                          </div>
                                          <div class="right-block">
                                              <h5 class="product-name"><a href="#">Maecenas consequat mauris</a></h5>
                                              <div class="product-star">
                                                  <i class="fa fa-star"></i>
                                                  <i class="fa fa-star"></i>
                                                  <i class="fa fa-star"></i>
                                                  <i class="fa fa-star"></i>
                                                  <i class="fa fa-star-half-o"></i>
                                              </div>
                                              <div class="content_price">
                                                  <span class="price product-price">$38,95</span>
                                                  <span class="price old-price">$52,00</span>
                                              </div>
                                          </div>
                                      </div>
                                  </li>
                              </ul>
                          </div> --}}
                          <!-- ./box product -->
                          <!-- box product -->
                          {{-- <div class="page-product-box">
                              <h3 class="heading">You might also like</h3>
                              <ul class="product-list owl-carousel" data-dots="false" data-loop="true" data-nav = "true" data-margin = "30" data-autoplayTimeout="1000" data-autoplayHoverPause = "true" data-responsive='{"0":{"items":1},"600":{"items":3},"1000":{"items":3}}'>
                                  <li>
                                      <div class="product-container">
                                          <div class="left-block">
                                              <a href="#">
                                                  <img class="img-responsive" alt="product" src="/assets/data/p97.jpg" />
                                              </a>
                                              <div class="quick-view">
                                                      <a title="Add to my wishlist" class="heart" href="#"></a>
                                                      <a title="Add to compare" class="compare" href="#"></a>
                                                      <a title="Quick view" class="search" href="#"></a>
                                              </div>
                                              <div class="add-to-cart">
                                                  <a title="Add to Cart" href="#add">Add to Cart</a>
                                              </div>
                                          </div>
                                          <div class="right-block">
                                              <h5 class="product-name"><a href="#">Maecenas consequat mauris</a></h5>
                                              <div class="product-star">
                                                  <i class="fa fa-star"></i>
                                                  <i class="fa fa-star"></i>
                                                  <i class="fa fa-star"></i>
                                                  <i class="fa fa-star"></i>
                                                  <i class="fa fa-star-half-o"></i>
                                              </div>
                                              <div class="content_price">
                                                  <span class="price product-price">$38,95</span>
                                                  <span class="price old-price">$52,00</span>
                                              </div>
                                          </div>
                                      </div>
                                  </li>
                                  <li>
                                      <div class="product-container">
                                          <div class="left-block">
                                              <a href="#">
                                                  <img class="img-responsive" alt="product" src="/assets/data/p34.jpg" />
                                              </a>
                                              <div class="quick-view">
                                                      <a title="Add to my wishlist" class="heart" href="#"></a>
                                                      <a title="Add to compare" class="compare" href="#"></a>
                                                      <a title="Quick view" class="search" href="#"></a>
                                              </div>
                                              <div class="add-to-cart">
                                                  <a title="Add to Cart" href="#add">Add to Cart</a>
                                              </div>
                                          </div>
                                          <div class="right-block">
                                              <h5 class="product-name"><a href="#">Maecenas consequat mauris</a></h5>
                                              <div class="product-star">
                                                  <i class="fa fa-star"></i>
                                                  <i class="fa fa-star"></i>
                                                  <i class="fa fa-star"></i>
                                                  <i class="fa fa-star"></i>
                                                  <i class="fa fa-star-half-o"></i>
                                              </div>
                                              <div class="content_price">
                                                  <span class="price product-price">$38,95</span>
                                                  <span class="price old-price">$52,00</span>
                                              </div>
                                          </div>
                                      </div>
                                  </li>
                                  <li>
                                      <div class="product-container">
                                          <div class="left-block">
                                              <a href="#">
                                                  <img class="img-responsive" alt="product" src="/assets/data/p36.jpg" />
                                              </a>
                                              <div class="quick-view">
                                                      <a title="Add to my wishlist" class="heart" href="#"></a>
                                                      <a title="Add to compare" class="compare" href="#"></a>
                                                      <a title="Quick view" class="search" href="#"></a>
                                              </div>
                                              <div class="add-to-cart">
                                                  <a title="Add to Cart" href="#add">Add to Cart</a>
                                              </div>
                                          </div>
                                          <div class="right-block">
                                              <h5 class="product-name"><a href="#">Maecenas consequat mauris</a></h5>
                                              <div class="product-star">
                                                  <i class="fa fa-star"></i>
                                                  <i class="fa fa-star"></i>
                                                  <i class="fa fa-star"></i>
                                                  <i class="fa fa-star"></i>
                                                  <i class="fa fa-star-half-o"></i>
                                              </div>
                                              <div class="content_price">
                                                  <span class="price product-price">$38,95</span>
                                                  <span class="price old-price">$52,00</span>
                                              </div>
                                          </div>
                                      </div>
                                  </li>
                                  <li>
                                      <div class="product-container">
                                          <div class="left-block">
                                              <a href="#">
                                                  <img class="img-responsive" alt="product" src="/assets/data/p36.jpg" />
                                              </a>
                                              <div class="quick-view">
                                                      <a title="Add to my wishlist" class="heart" href="#"></a>
                                                      <a title="Add to compare" class="compare" href="#"></a>
                                                      <a title="Quick view" class="search" href="#"></a>
                                              </div>
                                              <div class="add-to-cart">
                                                  <a title="Add to Cart" href="#add">Add to Cart</a>
                                              </div>
                                          </div>
                                          <div class="right-block">
                                              <h5 class="product-name"><a href="#">Maecenas consequat mauris</a></h5>
                                              <div class="product-star">
                                                  <i class="fa fa-star"></i>
                                                  <i class="fa fa-star"></i>
                                                  <i class="fa fa-star"></i>
                                                  <i class="fa fa-star"></i>
                                                  <i class="fa fa-star-half-o"></i>
                                              </div>
                                              <div class="content_price">
                                                  <span class="price product-price">$38,95</span>
                                                  <span class="price old-price">$52,00</span>
                                              </div>
                                          </div>
                                      </div>
                                  </li>
                              </ul>
                          </div> --}}
                          <!-- ./box product -->
                      </div>
                  <!-- Product -->
              </div>
              <!-- ./ Center colunm -->
          </div>
          <!-- ./row-->
      </div>
  </div>
@endsection

@section('footer')
  <script type="text/javascript" src="/assets/lib/jquery.bxslider/jquery.bxslider.min.js"></script>
  <script type="text/javascript" src="/assets/lib/owl.carousel/owl.carousel.min.js"></script>
  <script type="text/javascript" src="/assets/lib/jquery.countdown/jquery.countdown.min.js"></script>
  <script type="text/javascript" src="/assets/lib/jquery.elevatezoom.js"></script>

  <script type="text/javascript" src="/assets/lib/jquery-ui/jquery-ui.min.js"></script>

  <script type="text/javascript" src="/assets/lib/fancyBox/jquery.fancybox.js"></script>

  <script type="text/javascript" src="/assets/js/jquery.actual.min.js"></script>
@endsection
