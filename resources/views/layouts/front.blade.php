<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{$title}}</title>
    <link href="/dashboard_assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="/assets/lib/bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="/assets/lib/font-awesome/css/font-awesome.min.css" />
    <link rel="stylesheet" href="/dashboard_assets/plugins/bootstrap-toastr/toastr.min.css" >
    <link rel="stylesheet" type="text/css" href="/assets/css/animate.css" />
    <link rel="stylesheet" type="text/css" href="/assets/css/reset.css" />
    <link rel="stylesheet" type="text/css" href="/assets/css/style.css" />
    <link rel="stylesheet" type="text/css" href="/assets/css/responsive.css" />
     @yield('header')
     <link rel="stylesheet" href="/custome.css" >
</head>
<body class="category-page">
<!-- HEADER -->
<div id="header" class="header">
    <div class="top-header">
        <div class="container">
          <div class="language pull-left">
              <div class="dropdown">
                <?php $languages =  App\Language::all();
                $current = App\Language::where('code',App::getLocale())->get();
                 ?>
                    <a class="current-open" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
                    <img width="25"  src="{{$current[0]['logo']}}" />{{$current[0]['name']}}
                    </a>
                    <ul class="dropdown-menu" role="menu">
                      @foreach ($languages as $lang)
                        <li><a href="{{URL::to('/language?code='.$lang['code'])}}"><img width="20" src="{{$lang['logo']}}" />{{$lang['name']}}</a></li>
                      @endforeach
                  </ul>
              </div>
          </div>
            <div id="main-menu" class="col-sm-12 main-menu">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                <i class="fa fa-bars"></i>
                            </button>
                            <a class="navbar-brand" href="#">{{trans('front.menu_button_minimize')}}</a>
                        </div>
                        <div id="navbar" class="navbar-collapse collapse">
                            <ul class="nav navbar-nav">
                              <li class="active">
                                <a href="{{URL::to('/')}}">{{trans('front.nav_home')}}</a>
                              </li>

                              <li>
                                <a href="{{URL::to('/')}}">دایرکتوری شرکت ها</a>
                              </li>
                              <li>
                                <a href="{{URL::to('/')}}">دایرکتوری محصولات </a>
                              </li>
                              <li>
                                <a href="{{URL::to('/')}}">برند های معتبر</a>
                              </li>
                              <li>
                                <a href="{{URL::to('/')}}">آکهی محصولات</a>
                              </li>
                              <li>
                                <a href="{{URL::to('/')}}">راهنما</a>
                              </li>
                              <li>
                                <a href="{{URL::to('/')}}">معرفی پروژه</a>
                              </li>
                              <li>
                                <a href="{{URL::to('/contact')}}">{{trans('front.nav_contact')}}</a>
                              </li>
                            </ul>
                        </div><!--/.nav-collapse -->
                    </div>
                </nav>
            </div>
        </div>
    </div>
    <!--/.top-header -->
    <!-- MAIN HEADER -->
    <div class="container main-header">
        <div class="row">
          <div class="col-xs-12 col-sm-3 logo">
              <a href="{{URL::to('/')}}"><img alt="Kute shop - GFXFree.Net" src="/logo.png" width="80" /></a>
          </div>
          <div class="col-xs-12 col-sm-6 header-search-box">
              <form class="form-inline">
                    <div class="form-group input-serach">
                      <input type="text"  placeholder="{{trans('front.search_pholder')}}">
                    </div>
                    <button type="submit" class="pull-right btn-search"></button>
              </form>
          </div>
            <div id="user-info-top" class="user-info col-sm-3">
                <div class="dropdown menu_auth">
                    <a class="current-open" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#"><span>{{trans('front.menu_signin')}}</span></a>
                    <ul class="dropdown-menu mega_dropdown" role="menu">
                      <form method="post" action="{{URL::to('/auth')}}">
                       <label for="emmail_login">آدرس ایمیل</label>
                       <input id="emmail_login" type="text" name="email" class="form-control">
                       <label for="password_login">رمز عبور</label>
                       <input id="password_login" name="password" type="password" class="form-control">
                       <input type="hidden" name="guard" value="admin">
                       <br>
                       <button type="submit"  class="button"><i class="fa fa-lock"></i> ورود به حساب</button>
                      </form>
                    </ul>
                </div>

                <div class="dropdown menu_auth">
                    <a class="current-open" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#"><span>{{trans('front.menu_signup')}}</span></a>
                    <ul class="dropdown-menu mega_dropdown" role="menu">
                        <li><a href="{{URL::to('/auth/signup')}}">{{trans('front.menu_register_as_user')}}</a></li>
                        <li><a href="{{URL::to('/auth/company')}}">{{trans('front.menu_register_as_company')}}</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- END MANIN HEADER -->
    <div id="nav-top-menu" class="nav-top-menu">
        <div class="container">
            <div class="row">

            </div>
            <!-- userinfo on top-->
            <div id="form-search-opntop">
            </div>
            <!-- userinfo on top-->
            <div id="user-info-opntop">
            </div>
            <!-- CART ICON ON MMENU -->
            <div id="shopping-cart-box-ontop">
                <i class="fa fa-shopping-cart"></i>
                <div class="shopping-cart-box-ontop-content"></div>
            </div>
        </div>
    </div>
</div>
<!-- end header -->
<!-- page wapper-->

        @yield('body')
<!-- ./page wapper-->
<!-- Footer -->
<footer id="footer">
     <div class="container">
            <!-- introduce-box -->
            <div id="introduce-box" class="row">
              <div class="col-md-6">
                <ul class="foote_languages">
                  @foreach (\App\Helper\B2b::langs() as $lang)
                      <li><a href="#">{{$lang->name}}</a></li>
                  @endforeach
                </ul>
              </div>
              <div class="col-md-6">
                <h4 class="pull-right">: ما را دنبال کنید</h4>
                <ul class="footer_social pull-left">
                  <li><i class="fa fa-instagram"></i></li>
                  <li><i class="fa fa-facebook-square"></i></li>
                  <li><i class="fa fa-linkedin-square"></i></li>
                  <li><i class="fa fa-google-plus-square"></i></li>

                </ul>
              </div>
              <div class="col-md-12">
                <br>
                    <hr>
                    <br>
              </div>
              <div class="col-md-3">
                <div id="address-list">
                    <div class="tit-name">{{trans('front.footer_address_title')}}</div>
                    <div class="tit-contain">{{trans('front.footer_address')}}</div>
                    <div class="tit-name">{{trans('front.footer_phone_title')}}</div>
                    <div class="tit-contain">{{trans('front.footer_phone')}}</div>
                    <div class="tit-name">{{trans('front.footer_email_title')}}</div>
                    <div class="tit-contain">{{trans('front.footer_email')}}</div>
                </div>
              </div>
              <div class="col-md-9">
                <ul class="footer_menu">
                  <li><a href="#">صفحه اصلی</a></li>
                  <li><a href="#">صفحه اصلی</a></li>
                  <li><a href="#">صفحه اصلی</a></li>
                  <li><a href="#">صفحه اصلی</a></li>
                  <li><a href="#">صفحه اصلی</a></li>
                  <li><a href="#">صفحه اصلی</a></li>
                  <li><a href="#">صفحه اصلی</a></li>
                  <li><a href="#">صفحه اصلی</a></li>
<br>
<li><a href="#">صفحه اصلی</a></li>
<li><a href="#">صفحه اصلی</a></li>
<li><a href="#">صفحه اصلی</a></li>
<li><a href="#">صفحه اصلی</a></li>
<li><a href="#">صفحه اصلی</a></li>
<li><a href="#">صفحه اصلی</a></li>

<br>    <li><a href="#">صفحه اصلی</a></li>
<li><a href="#">صفحه اصلی</a></li>
<li><a href="#">صفحه اصلی</a></li>
                </ul>
              </div>
              {{-- <div class="col-md-4">
                  <div id="address-box">
                      <a href="#"><img src="/logo.png" alt=""  width="100"/></a>
                  </div>
              </div>
              <div class="col-md-4">
                <div id="address-list">
                    <div class="tit-name">{{trans('front.footer_address_title')}}</div>
                    <div class="tit-contain">{{trans('front.footer_address')}}</div>
                    <div class="tit-name">{{trans('front.footer_phone_title')}}</div>
                    <div class="tit-contain">{{trans('front.footer_phone')}}</div>
                    <div class="tit-name">{{trans('front.footer_email_title')}}</div>
                    <div class="tit-contain">{{trans('front.footer_email')}}</div>
                </div>
              </div>
              <div class="col-sm-4" style="direction:rtl">
                  <div class="introduce-title">منوی سایت</div>
                  <ul id="introduce-company"  class="introduce-list">
                    <li><a href="{{URL::to('/')}}">{{trans('front.nav_home')}}</a></li>
                    <li><a href="{{URL::to('/auth')}}">{{trans('front.menu_login')}}</a></li>
                    <li><a href="{{URL::to('/auth/signup')}}">{{trans('front.menu_register_as_user')}}</a></li>
                    <li><a href="{{URL::to('/auth/company')}}">{{trans('front.menu_register_as_company')}}</a></li>
                  </ul>
              </div> --}}
            </div><!-- /#introduce-box -->
            <div id="footer-menu-box">
                <p class="text-center">Copyrights &#169; 2015 KuteShop. All Rights Reserved.</p>
            </div><!-- /#footer-menu-box -->
        </div>
</footer>

<a href="#" class="scroll_top" title="Scroll to Top" style="display: inline;">Scroll</a>
<!-- Script-->
<script type="text/javascript" src="/assets/lib/jquery/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="/assets/lib/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/assets/lib/select2/js/select2.min.js"></script>
<script type="text/javascript" src="/dashboard_assets/plugins/bootstrap-toastr/toastr.min.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() {
toastr.options = {
"closeButton": true,
"debug": false,
"positionClass": "toast-top-right",
"showDuration": "1000",
"hideDuration": "1000",
"timeOut": "5000",
"extendedTimeOut": "1000",
"showEasing": "swing",
"hideEasing": "linear",
"showMethod": "fadeIn",
"hideMethod": "fadeOut"
};
@if(Session::has('flash.alerts'))
@foreach(Session::get('flash.alerts') as $alert)
@if ( $alert['level']  == 'danger')
toastr.error("{{$alert['message']}}");
@else
toastr.{{ $alert['level'] }}("{{$alert['message']}}");
@endif
@endforeach
@endif
});
</script>
 @yield('footer')
</body>
</html>
