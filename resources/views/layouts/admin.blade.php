<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8">
<title>{{$title}}</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport">
<meta content="" name="description">
<meta content="" name="author">
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<!-- <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"> -->
<link href="/dashboard_assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="/dashboard_assets/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
<link href="/dashboard_assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="/dashboard_assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="/dashboard_assets/plugins/bootstrap-toastr/toastr.min.css" >
<!-- END GLOBAL MANDATORY STYLES -->
<link href="/dashboard_assets/css/components-rounded.css" id="style_components" rel="stylesheet" type="text/css">
<link href="/dashboard_assets/css/plugins.css" rel="stylesheet" type="text/css">
<link href="/dashboard_assets/css/layout.css" rel="stylesheet" type="text/css">
<link href="/dashboard_assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color">
<link href="/custome_dashboard.css" rel="stylesheet" type="text/css">
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="/favicon.ico">
<link rel="stylesheet" href="/custome.css" >

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-menu-fixed" class to set the mega menu fixed  -->
<!-- DOC: Apply "page-header-top-fixed" class to set the top menu fixed  -->
@yield('header')
<body>
	<?php $admin = \App\Helper\B2b::user('admin'); ?>
<!-- BEGIN HEADER -->
<div class="page-header">
	<!-- BEGIN HEADER TOP -->
	<div class="page-header-top">
		<div class="container">
			<!-- BEGIN LOGO -->
			<div class="page-logo">
				<a href="{{URL::to('/')}}"><img src="/logo.png" width="100" alt="logo" class="logo-default"></a>
			</div>
			<!-- END LOGO -->
			<!-- BEGIN RESPONSIVE MENU TOGGLER -->
			<a href="javascript:;" class="menu-toggler"></a>
			<!-- END RESPONSIVE MENU TOGGLER -->
			<!-- BEGIN TOP NAVIGATION MENU -->
			<div class="top-menu">
				<ul class="nav navbar-nav pull-right">
					<li class="dropdown dropdown-user dropdown-dark">
						<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
							<span class="username username-hide-mobile">{{$admin->first_name .' '.$admin->last_name}}</span>
						<img alt="" class="img-circle" src="{{App\Helper\B2b::gravatar($admin->email)}}">
						</a>
						<ul class="dropdown-menu dropdown-menu-default">
							<li>
								<a href="{{URL::to('/dashboard/profile')}}">
								<i class="icon-user"></i> پروفایل کاربر </a>
							</li>
							<li class="divider">
							</li>
							<li>
								<a href="{{URL::to('/auth/signout')}}">
								<i class="icon-key"></i>خروج</a>
							</li>
						</ul>
					</li>
					<!-- END USER LOGIN DROPDOWN -->
				</ul>
			</div>
			<!-- END TOP NAVIGATION MENU -->
		</div>
	</div>
	<!-- END HEADER TOP -->
	<!-- BEGIN HEADER MENU -->
	<div class="page-header-menu">
		<div class="container">
			<!-- BEGIN HEADER SEARCH BOX -->
			<form class="search-form" action="extra_search.html" method="GET">
				<div class="input-group">
					<input type="text" class="form-control" placeholder="Search" name="query">
					<span class="input-group-btn">
					<a href="javascript:;" class="btn submit"><i class="icon-magnifier"></i></a>
					</span>
				</div>
			</form>
			<!-- END HEADER SEARCH BOX -->
			<!-- BEGIN MEGA MENU -->
			<!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
			<!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
			<div class="hor-menu ">
				<ul class="nav navbar-nav">
					<li class="active">
						<a href="{{URL::to('/dashboard')}}">داشبورد</a>
					</li>
					<li>
						<a href="{{URL::to('/dashboard/products')}}">محصولات</a>
					</li>
					<li>
						<a href="{{URL::to('/dashboard/orders')}}">سفارشات</a>
					</li>
					<li>
						<a href="{{URL::to('/dashboard/messages')}}">پیام ها</a>
					</li>
					<li>
						<a href="{{URL::to('/dashboard/companies')}}">شرکت ها</a>
					</li>
					<li>
						<a href="{{URL::to('/dashboard/users')}}">کاربران</a>
					</li>
					<li>
						<a href="{{URL::to('/dashboard/admins')}}">مدیران</a>
					</li>
					<li>
						<a href="{{URL::to('/dashboard/transactions')}}">تراکنش ها</a>
					</li>
				</ul>
			</div>
			<!-- END MEGA MENU -->
		</div>
	</div>
	<!-- END HEADER MENU -->
</div>
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
			@yield('body')
		</div>
<!-- END PAGE CONTAINER -->
<!-- BEGIN PRE-FOOTER -->
<div class="page-prefooter">
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-sm-6 col-xs-12 footer-block">
				<h2>About</h2>
				<p>
					 Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam dolore.
				</p>
			</div>
			<div class="col-md-3 col-sm-6 col-xs12 footer-block">
				<h2>Subscribe Email</h2>
				<div class="subscribe-form">
					<form action="javascript:;">
						<div class="input-group">
							<input type="text" placeholder="mail@email.com" class="form-control">
							<span class="input-group-btn">
							<button class="btn" type="submit">Submit</button>
							</span>
						</div>
					</form>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-12 footer-block">
				<h2>Follow Us On</h2>
				<ul class="social-icons">
					<li>
						<a href="javascript:;" data-original-title="rss" class="rss"></a>
					</li>
					<li>
						<a href="javascript:;" data-original-title="facebook" class="facebook"></a>
					</li>
					<li>
						<a href="javascript:;" data-original-title="twitter" class="twitter"></a>
					</li>
					<li>
						<a href="javascript:;" data-original-title="googleplus" class="googleplus"></a>
					</li>
					<li>
						<a href="javascript:;" data-original-title="linkedin" class="linkedin"></a>
					</li>
					<li>
						<a href="javascript:;" data-original-title="youtube" class="youtube"></a>
					</li>
					<li>
						<a href="javascript:;" data-original-title="vimeo" class="vimeo"></a>
					</li>
				</ul>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-12 footer-block">
				<h2>Contacts</h2>
				<address class="margin-bottom-40">
				Phone: 800 123 3456<br>
				 Email: <a href="mailto:info@metronic.com">info@metronic.com</a>
				</address>
			</div>
		</div>
	</div>
</div>
<!-- END PRE-FOOTER -->
<!-- BEGIN FOOTER -->
<div class="page-footer">
	<div class="container">
		 2014 &copy; Metronic by keenthemes. <a href="http://themeforest.net/item/metronic-responsive-admin-dashboard_assets-template/4021469?ref=keenthemes" title="Purchase Metronic just for 27$ and get lifetime updates for free" target="_blank">Purchase Metronic!</a>
	</div>
</div>
<div class="scroll-to-top">
	<i class="icon-arrow-up"></i>
</div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS (Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="/dashboard_assets/global/plugins/respond.min.js"></script>
<script src="/dashboard_assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="/dashboard_assets/plugins/jquery.min.js" type="text/javascript"></script>
<script src="/dashboard_assets/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="/dashboard_assets/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="/dashboard_assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/dashboard_assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="/dashboard_assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="/dashboard_assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="/dashboard_assets/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="/dashboard_assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script type="text/javascript" src="/dashboard_assets/plugins/bootstrap-toastr/toastr.min.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() {
toastr.options = {
"closeButton": true,
"debug": false,
"positionClass": "toast-top-right",
"showDuration": "1000",
"hideDuration": "1000",
"timeOut": "5000",
"extendedTimeOut": "1000",
"showEasing": "swing",
"hideEasing": "linear",
"showMethod": "fadeIn",
"hideMethod": "fadeOut"
};
@if(Session::has('flash.alerts'))
@foreach(Session::get('flash.alerts') as $alert)
@if ( $alert['level']  == 'danger')
toastr.error("{{$alert['message']}}");
@else
toastr.{{ $alert['level'] }}("{{$alert['message']}}");
@endif
@endforeach
@endif
});
</script>
@yield('footer')
</body>
<!-- END BODY -->
</html>
