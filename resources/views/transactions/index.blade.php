@extends('layouts.admin')
@section('header')
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="/dashboard_assets/plugins/dataTables/plugins/bootstrap/dataTables.bootstrap.css"/>
<!-- END PAGE LEVEL STYLES -->
@endsection
@section('body')
<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>مدیریت تراکنش ها</h1>
    </div>
    <br>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
<div class="row">
  <div class="col-md-12">
    <!-- BEGIN SAMPLE TABLE PORTLET-->
    <div class="portlet light">
      <div class="portlet-body">
        <div class="table-scrollable">
          <table class="table table-hover" id="datatable">
          </table>
        </div>
      </div>
    </div>
    <!-- END SAMPLE TABLE PORTLET-->
  </div>
</div>
@stop
@section('footer')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="/dashboard_assets/plugins/dataTables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/dashboard_assets/plugins/dataTables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<script type="text/javascript">
var table = $('#datatable').DataTable({
				"processing": true,
				"serverSide": true,
				"stateSave": true,
        "language": {
                "url": "/Persian.json"
            },
				"ajax": {
					'type': 'post',
					'url': "{{URL::to('/dashboard/transactions')}}"
				},
				"columns": [    { "data": "id",   "targets": 0 ,"title":"شناسه تراکنش"},
		{ "data": "owner",  "targets": 1 ,'title':'واریز کننده'},
    { "data": "amount", "targets": 2 ,'title':'مقدار'},
    { "data": "date", "targets": 4 ,'title':'تاریخ'},
		{ "data": "status", "targets": 5 ,'title':'وضیعت'},
    { "data": "action",  "targets": 6,'sortable':false},],
	});
 $(document).on('click','.confirmation', function () {
        return confirm('Are you sure?');
    });
</script>
@stop
