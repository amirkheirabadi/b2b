@extends('layouts.admin')

@section('header')
  <link href="/dashboard_assets/pages/css/invoice.css" rel="stylesheet" type="text/css"/>
@endsection

@section('body')
  <!-- BEGIN PAGE CONTAINER -->
  <div class="page-container">
  	<!-- BEGIN PAGE HEAD -->
  	<div class="page-head">
  		<div class="container">
  			<!-- BEGIN PAGE TITLE -->
  			<div class="page-title">
  				<h1>نمایش تراکنش</h1>
  			</div>
  		</div>
  	</div>
  	<!-- END PAGE HEAD -->
  	<!-- BEGIN PAGE CONTENT -->
  	<div class="page-content">
  		<div class="container" id="rtl_layout">
  			<!-- BEGIN PAGE CONTENT INNER -->
  			<div class="portlet light">
  				<div class="portlet-body">
  					<div class="invoice">
  						<div class="row invoice-logo">
  							<div class="col-xs-6 invoice-logo-space">
  								<img src="/logo.jpg" class="img-responsive" alt=""/>
  							</div>
  							<div class="col-xs-6">
  								<p>
  									 #{{$trans->id}} / {{$trans->created_at->toFormattedDateString()}}
  								</p>
  							</div>
  						</div>
  						<hr/>
  						<div class="row">
  							<div class="col-xs-6">
  								<h3>شرکت:</h3>
  								<ul class="list-unstyled">
                    <li>
                       #{{$trans->company->id}}
                    </li>
  									<li>
                    {{$trans->company->name}}
  									</li>
  									<li>.
  										 {{$trans->company->owner}}
  									</li>
  									<li>
  										 {{$trans->company->email}}
  									</li>
  									<li>
  										{{$trans->company->phone}}
  									</li>
  									<li>
                      {{$trans->company->website}}
  									</li>
  								</ul>
  							</div>
  							<div class="col-xs-6 invoice-payment">
  								<h3>جزئیات پرداخت :</h3>
  								<ul class="list-unstyled">
  									<li>
  										<strong>در تاریخ:</strong> {{$trans->created_at->toFormattedDateString()}}
  									</li>
  									<li>
  										<strong>Ref:</strong> {{$trans->refnum}}
  									</li>
  									<li>
  										<strong>Res:</strong> {{$trans->resnum}}
  									</li>
  								</ul>
  							</div>
  						</div>
  						<div class="row">
  							<div class="col-xs-12">
  								<table class="table table-striped table-hover">
  								<tbody>
  								<tr>
  									<td>
                      <?php
                      switch ($trans->plan) {
                        case 'silver':
                          echo 'نقره ای';
                          break;

                        case 'gold':
                          echo 'طلایی';
                          break;
                      }
                       ?>
  									</td>
  									<td class="hidden-480">
                    {{$trans->period}} ماه
  									</td>
  									<td class="hidden-480">
  										 {{$trans->price}} ریال
  									</td>
  								</tr>
  								</tbody>
  								</table>
  							</div>
  						</div>
  						<div class="row">
  							<div class="col-xs-4">
  								<div class="well">
  									<address>
  									<strong>آدرس</strong><br/>
  									چهارراه ابوطلالب<br/>
  									ایران - مشهد<br/>
  									<abbr title="Phone">P:</abbr> (98) 145-1810 </address>
  									<address>
  									<strong>امور مالی</strong><br/>
  									<a href="mailto:#">
  									info@email.com </a>
  									</address>
  								</div>
  							</div>
  							<div class="col-xs-8 invoice-block">
  								<ul class="list-unstyled amounts">
  									<li>
  										<strong>مبلغ نهایی:</strong> {{$trans->price}} ریال
  									</li>
  									<li>
  										<strong>وضعیت :</strong>
                      <?php
                      switch ($trans->status) {
                        case 'pending':
                        echo 'در انتظار پرداخت';
                          break;

                      case 'success':
                        echo 'پرداخت شده';
                        break;
                      }
                       ?>
  									</li>
  								</ul>
  								<br/><br><br>
  								<a class="btn btn-lg blue hidden-print margin-bottom-5" onclick="javascript:window.print();">
  								چاپ صورتحساب <i class="fa fa-print"></i>
  								</a>
  							</div>
  						</div>
  					</div>
  				</div>
  			</div>
  			<!-- END PAGE CONTENT INNER -->
  		</div>
  	</div>
  	<!-- END PAGE CONTENT -->
  </div>
  <!-- END PAGE CONTAINER -->
@endsection

@section('footer')

@endsection
