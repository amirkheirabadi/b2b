@extends('layouts.admin')
@section('header')
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="/dashboard_assets/plugins/dataTables/plugins/bootstrap/dataTables.bootstrap.css"/>
<!-- END PAGE LEVEL STYLES -->
@endsection
@section('body')
<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>مدیریت محصولات</h1>
    </div>
    <!-- END PAGE TITLE -->
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
<div class="row">
  <div class="col-md-12">
    <!-- BEGIN SAMPLE TABLE PORTLET-->
    <div class="portlet light">
      <div class="portlet-body">
        <div class="table-scrollable">
          <table class="table table-hover" id="datatable">
          </table>
        </div>
      </div>
    </div>
    <!-- END SAMPLE TABLE PORTLET-->
  </div>
</div>
@stop
@section('footer')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="/dashboard_assets/plugins/dataTables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/dashboard_assets/plugins/dataTables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<script type="text/javascript">
var table = $('#datatable').DataTable({
				"processing": true,
				"serverSide": true,
				"stateSave": true,
				"ajax": {
					'type': 'post',
					'url': "{{URL::to('/dashboard/products')}}"
				},
        "language": {
                "url": "{{URL::to('/dashboard_assets/Persian.json')}}"
        },
				"columns": [    { "data": "id",   "targets": 0 ,"title":"شماره محصول"},
		{ "data": "name",  "targets": 1 ,'title':'نام محصول'},
		{ "data": "company", "targets": 2 ,'title':'شرکت','sortable':false},
    { "data": "action",  "targets": 4,'sortable':false},],
	});
 $(document).on('click','.confirmation', function () {
        return confirm('Are you sure?');
    });
</script>
@stop
