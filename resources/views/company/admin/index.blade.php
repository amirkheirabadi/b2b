@extends('layouts.admin')
@section('header')
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="/dashboard_assets/plugins/dataTables/plugins/bootstrap/dataTables.bootstrap.css"/>
<!-- END PAGE LEVEL STYLES -->
@endsection
@section('body')
<!-- BEGIN PAGE HEAD -->
<div class="page-head">
  <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
      <h1>مدیریت شرکت ها</h1>
    </div>
    <!-- END PAGE TITLE -->
    <br>
    <a href="{{URL::to('/dashboard/companies/create')}}" type="button" class="btn btn-primary">ایجاد شرکت جدید</a>
  </div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="container">
<div class="row">
  <div class="col-md-12">
    <!-- BEGIN SAMPLE TABLE PORTLET-->
    <div class="portlet light">
      <div class="portlet-body">
        <div class="table-scrollable">
          <table class="table table-hover" id="datatable">
          </table>
        </div>
      </div>
    </div>
    <!-- END SAMPLE TABLE PORTLET-->
  </div>
</div>
@stop
@section('footer')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="/dashboard_assets/plugins/dataTables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/dashboard_assets/plugins/dataTables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<script type="text/javascript">
var table = $('#datatable').DataTable({
				"processing": true,
				"serverSide": true,
				"stateSave": true,
				"ajax": {
					'type': 'post',
					'url': "{{URL::to('/dashboard/companies')}}"
				},
				"columns": [    { "data": "name",   "targets": 0 ,"title":"نام"},
		{ "data": "email",  "targets": 1 ,'title':'مدیرعامل'},
		{ "data": "mobile", "targets": 2 ,'title':'ایمیل'},
		{ "data": "status", "targets": 3 ,'title':'وضعیت'},
    { "data": "action",  "targets": 4,'sortable':false},],
			});

 $(document).on('click','.confirmation', function () {
        return confirm('Are you sure?');
    });
</script>
@stop
