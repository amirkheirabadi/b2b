@extends('layouts.front')


@section('body')
  <div class="columns-container">
      <div class="container auth_container" id="columns">
  <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="#" title="Return to Home">صفحه اصلی</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">ورود به سیستم</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- page heading-->
        <h2 class="page-heading">
            <span class="page-heading-title2">اعتبار سنجی کاربر</span>
        </h2>
        <!-- ../page heading-->
        <div class="page-content">
            <div class="row">
                <div class="col-sm-6">
                    <div class="box-authentication">
                       <form method="post" action="{{URL::to('/auth')}}">
                        <h3>ورود به حساب کاربری </h3>
                        <label for="emmail_login">آدرس ایمیل</label>
                        <input id="emmail_login" type="text" name="email" class="form-control">
                        <label for="password_login">رمز عبور</label>
                        <input id="password_login" name="password" type="password" class="form-control">
                        <p class="forgot-pass"><a href="{{URL::to('/auth/reset')}}">رمز عبور خود را فراموش کرده اید ؟</a></p>
                        <input type="hidden" name="guard" value="user">
                        <button type="submit"  class="button"><i class="fa fa-lock"></i> ورود به حساب</button>
                       </form>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="box-authentication">
                        <form method="post" action="{{URL::to('/auth')}}">
                        <h3>ورود به حساب شرکت</h3>
                        <label for="emmail_login">آدرس ایمیل شرکت</label>
                        <input id="emmail_login" name="email" type="text" class="form-control">
                        <label for="password_login">رمز عبور</label>
                        <input id="password_login" name="password"  type="password" class="form-control">
                        <p class="forgot-pass"><a href="{{URL::to('/auth/companty_reset')}}">رمز عبور خود را فراموش کرده اید ?</a></p>
                          <input type="hidden" name="guard" value="company">
                        <button  type="submit" class="button"><i class="fa fa-lock"></i> ورود به حساب</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
@stop
