@extends('layouts.front')

@section('body')
  <div class="columns-container">
      <div class="container" id="columns">
  <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="#" title="Return to Home">صفحه اصلی</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">ثبت نام در سیستم</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- page heading-->
        <h2 class="page-heading">
            <span class="page-heading-title2">ثبت نام کاربر</span>
        </h2>
        <!-- ../page heading-->
        <div class="page-content">
            <div class="row">
                <div class="col-sm-12">
                    <div class="box-authentication">
                       <form method="post" action="{{URL::to('/auth/signup')}}">
                        <label for="emmail_login">نام</label>
                        <input id="emmail_login" type="text" name="fname" class="form-control">

                        <label for="emmail_login">نام خانوادگی</label>
                        <input id="emmail_login" type="text" name="lname" class="form-control">

                        <label for="emmail_login">آدرس ایمیل</label>
                        <input id="emmail_login" type="text" name="email" class="form-control">

                        <label for="password_login">رمز عبور</label>
                        <input id="password_login" name="password" type="password" class="form-control">

                        <label for="password_login">تکرار رمز عبور</label>
                        <input id="password_login" name="password_confirmation" type="password" class="form-control">

                        <p class="forgot-pass"><a href="{{URL::to('/auth')}}"> قبلا در سیستم عضو شده اید ؟</a></p>
                        <input type="hidden" name="guard" value="user">
                        <button type="submit"  class="button"><i class="fa fa-lock"></i> &nbspثبت نام در سیستم</button>
                       </form>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
@stop
