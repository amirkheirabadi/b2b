@extends('layouts.front')


@section('body')
  <div class="columns-container">
      <div class="container" id="columns">
  <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="#" title="Return to Home">صفحه اصلی</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">فراموشی رمز عبور</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- page heading-->
        <h2 class="page-heading">
            <span class="page-heading-title2">ارسال کد فعال سازی</span>
        </h2>
        <!-- ../page heading-->
        <div class="page-content">
            <div class="row">
                <div class="col-sm-12">
                    <div class="box-authentication">
                       <form method="post" action="{{URL::to('/auth/companyreconfirmation')}}">
                        <label for="emmail_login">آدرس ایمیل</label>
                        <input id="emmail_login" type="text" name="email" class="form-control">

                        <p class="forgot-pass"><a href="{{URL::to('/auth')}}"> قبلا در سیستم عضو شده اید ؟</a></p>
                        <input type="hidden" name="guard" value="user">
                        <button type="submit"  class="button"><i class="fa fa-lock"></i>تغییر رمز عبور</button>
                       </form>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
@stop
