@extends('layouts.front')


@section('body')
  <div class="columns-container">
      <div class="container" id="columns">
  <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="#" title="Return to Home">صفحه اصلی</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">ثبت نام در سیستم</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- page heading-->
        <h2 class="page-heading">
            <span class="page-heading-title2">ثبت شرکت</span>
        </h2>
        <!-- ../page heading-->
        <div class="page-content">
            <div class="row">
                <div class="col-sm-12">
                    <div class="box-authentication rtl_layout">
                       <form method="post" action="{{URL::to('/auth/company')}}">
                        <label for="emmail_login">نام شرکت</label>
                        <input id="emmail_login" type="text" name="name" class="form-control">

                        <label for="emmail_login">نام مدیر عامل</label>
                        <input id="emmail_login" type="text" name="owner" class="form-control">

                        <label for="emmail_login">آدرس ایمیل شرکت</label>
                        <input id="emmail_login" type="text" name="email" class="form-control">

                        <label for="emmail_login">تلفن شرکت</label>
                        <input id="emmail_login" type="text" name="phone" class="form-control">

                        <label for="emmail_login">گروه کاری</label>
                        <select class="form-control" name="group">
                          @foreach ($groups as $group)
                              <?php $group->trans() ?>

                              <option value="{{$group->id}}">{{$group->name}}</option>
                          @endforeach
										    </select>

                        <div class="form-group">
										<label>مناطق جغرافیایی</label>
										<div class="checkbox-list">
                      @foreach ($scopes as $scope)
                        <?php $scope->trans() ?>
                        <label class="checkbox-inline">
                        <div class="checker" id="uniform-inlineCheckbox1"><span class="checked"><input type="checkbox" name='scopes[]' id="inlineCheckbox1" value="{{$scope->id}}"></span></div> {{$scope->name}} </label>
                      @endforeach
                    </div>
									         </div>
                        <label for="password_login">رمز عبور</label>
                        <input id="password_login" name="password" type="password" class="form-control">

                        <label for="password_login">تکرار رمز عبور</label>
                        <input id="password_login" name="password_confirmation" type="password" class="form-control">

                        <p class="forgot-pass"><a href="{{URL::to('/auth')}}"> قبلا شرکت خود را ثبت کرده اید ؟</a></p>
                        <input type="hidden" name="guard" value="user">
                        <button type="submit"  class="button"><i class="fa fa-lock"></i>&nbsp&nbsp ثبت شرکت در سیستم</button>
                       </form>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
@stop
