@extends('layouts.front')


@section('body')
  <div class="columns-container">
      <div class="container" id="columns">
  <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="#" title="Return to Home">صفحه اصلی</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">تغییر رمز عبور</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- page heading-->
        <h2 class="page-heading">
            <span class="page-heading-title2">تغییر رمز عبور</span>
        </h2>
        <!-- ../page heading-->
        <div class="page-content">
            <div class="row">
                <div class="col-sm-12">
                    <div class="box-authentication">
                       <form method="post" action="{{URL::to('/auth/password')}}">
                        <label for="emmail_login">رمز عبور</label>
                        <input id="emmail_login" type="text" name="password" class="form-control">

                        <label for="emmail_login">تکرار رمز عبور</label>
                        <input id="emmail_login" type="text" name="password_confirmation" class="form-control">


                        <input type="hidden" name="email" value="{{$email}}">
                        <input type="hidden" name="reset" value="{{$reset}}">
                        <p class="forgot-pass"><a href="{{URL::to('/auth')}}"> قبلا در سیستم عضو شده اید ؟</a></p>
                        <input type="hidden" name="guard" value="user">
                        <button type="submit"  class="button"><i class="fa fa-lock"></i>تغییر رمز عبور</button>
                       </form>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
@stop
