@extends('layouts.front')


@section('body')
  <div class="columns-container auth_container">
      <div class="container" id="columns">
  <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="#" title="Return to Home">صفحه اصلی</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page">ورود به سیستم</span>
        </div>
        <!-- ./breadcrumb -->
        <!-- page heading-->
        <h2 class="page-heading">
            <span class="page-heading-title2">اعتبار سنجی کاربر</span>
        </h2>
        <!-- ../page heading-->
        <div class="page-content">
            <div class="row">
                <div class="col-sm-12">
                    <div class="box-authentication">
                       <form method="post" action="{{URL::to('/auth')}}">
                        <h3>ورود به حساب مدیر</h3>
                        <label for="emmail_login">آدرس ایمیل</label>
                        <input id="emmail_login" type="text" name="email" class="form-control">
                        <label for="password_login">رمز عبور</label>
                        <input id="password_login" name="password" type="password" class="form-control">
                        <input type="hidden" name="guard" value="admin">
                        <button type="submit"  class="button"><i class="fa fa-lock"></i> ورود به حساب</button>
                       </form>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
@stop
