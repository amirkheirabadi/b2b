کاربر محترم شرکت {{$name}}

برای تایید حساب کاربری خود بروی لینک زیر کلیک بفرمایید .
<a href="{{URL::to('/auth/companyconfirmation/'.$email.'/'.$confirmation_code)}}">تایید حساب کاربری</a>
