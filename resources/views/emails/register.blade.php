کاربر محترم  {{$name}}

برای تایید حساب کاربری خود بروی لینک زیر کلیک بفرمایید .
<a href="{{URL::to('/auth/confirmation/'.$email.'/'.$confirmation_code)}}">تایید حساب کاربری</a>
