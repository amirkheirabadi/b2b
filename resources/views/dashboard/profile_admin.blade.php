@extends('layouts.admin')

@section('header')
  <link href="/dashboard_assets/pages/css/profile.css" rel="stylesheet" type="text/css"/>
@stop

@section('body')
  <!-- BEGIN PAGE HEAD -->
  <div class="page-head">
    <div class="container">
      <!-- BEGIN PAGE TITLE -->
      <div class="page-title">
        <h1>پروفایل مدیر</h1>
      </div>
      <!-- END PAGE TITLE -->
    </div>
  </div>
  <!-- END PAGE HEAD -->
  <!-- BEGIN PAGE CONTENT -->
  <div class="page-content">
    <div class="container">
            <div class="row margin-top-10">
  				<div class="col-md-12">
  					<div class="profile-content">
  						<div class="row">
  							<div class="col-md-12">
  								<div class="portlet light">
  									<div class="form-body">
                      <form role="form" method="post" action="{{URL::to('/dashboard/profile')}}">
                        <div class="form-group">
                          <div class="col-md-6">
                            <label class="control-label">نام</label>
                            <input type="text" name="first_name" class="form-control" value="{{$admin->first_name}}">
                          </div>

                          <div class="col-md-6">
                            <label class="control-label">نام خانوادگی</label>
                            <input type="text" name="last_name" class="form-control" value="{{$admin->last_name}}">
                          </div>
                        </div>
                        &nbsp<br>
                        <div class="form-group">
                          <label class="control-label">آدرس ایمیل</label>
                          <input type="email" name="email" class="form-control" value="{{$admin->email}}">
                        </div>
&nbsp<br>
                        <div class="form-group">
                          <div class="col-md-6">
                            <label class="control-label">رمز عبور جدید</label>
                            <input type="password" name="password" class="form-control">
                          </div>

                          <div class="col-md-6">
                            <label class="control-label">تکرار رمز عبور جدید</label>
                            <input type="password" name="password_confirmation" class="form-control">
                          </div>
                        </div>
                        &nbsp<br><br>
                        <div class="margiv-top-10">
                          <button type="submit" class="btn green-haze">
                          ذخیره تغییرات </button>
                        </div>
                      </form>
  									</div>
  								</div>
  							</div>
  						</div>
  					</div>
  					<!-- END PROFILE CONTENT -->
  				</div>
  			</div>
      @stop
