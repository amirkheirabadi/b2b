@extends('layouts.company')

@section('body')

  @if ($company->plan == "free")
    <!-- BEGIN PAGE HEAD -->
    <div class="page-head">
      <div class="container">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
          <h1>داشبورد</h1>
        </div>
        <!-- END PAGE TITLE -->
      </div>
    </div>
    <!-- END PAGE HEAD -->
    <!-- BEGIN PAGE CONTENT -->
    <div class="page-content">
      <div class="container">
<div class="row">

  <div class="col-md-12">

    <div class="note note-warning note-bordered">
						<h4 class="block">حساب کاربری</h4>
						<p>
                شما از حساب رایگان استفاده می کنید شما به راحتی می توانید با خرید حساب های پریمیوم از امکانات ویژه ای برخوردار شوید .
						</p>
					</div>
					<!-- BEGIN SAMPLE TABLE PORTLET-->
					<div class="portlet light">
						<div class="portlet-body">
							<div class="table-scrollable table-scrollable-borderless">
								<table class="table table-hover table-light">
								<thead>
								<tr class="uppercase">
									<th >
										 نام
									</th>
									<th>
										 مدت
									</th>
									<th>
										 قیمت
									</th>
                  <th>

                  </th>
								</tr>
								</thead>
								<tbody>
								<tr>
									<td>
										<a href="javascript:;" class="primary-link">رایگان</a>
									</td>
                  <td>
                    -
                  </td>
									<td>
										 {{$price['free']}}  ریال
									</td>
								</tr>
								<tr>
                  <form class="" action="{{URL::to('/dashboard/account')}}" method="post">
                    <input type="hidden" name="plan" value="silver">
									<td>
										<a href="javascript:;" class="primary-link">نقره ای</a>
									</td>
									<td>
                   <select name="period" id="alert_close_in_seconds" class="form-control input-medium">
                     <option value="‍1">‍1 ماه</option>
                     <option value="3">3 ماه</option>
                     <option value="6">6 ماه</option>
                     <option value="12">1 سال</option>
                   </select>
									</td>
									<td>
										 {{$price['silver']}} ریال
									</td>
                  <td>
                    <button type="format" class="btn btn-primary">خرید</button>
                  </td>
								</tr>
              </form>
								<tr>
                  <form class="" action="{{URL::to('/dashboard/account')}}" method="post">
                    <input type="hidden" name="plan" value="gold">
									<td>
										<a href="javascript:;" class="primary-link">طلایی</a>
									</td>
									<td>
                    <select name="period" id="alert_close_in_seconds" class="form-control input-medium">
                      <option value="‍1">‍1 ماه</option>
                      <option value="3">3 ماه</option>
                      <option value="6">6 ماه</option>
                      <option value="12">1 سال</option>
                    </select>
									</td>
									<td>
                  {{$price['gold']}} ریال
									</td>
                  <td>
                    <button type="submit" class="btn btn-primary">خرید</button>
                  </td>
                </form>
								</tr>
								</tbody>
								</table>
							</div>
						</div>
					</div>
					<!-- END SAMPLE TABLE PORTLET-->
				</div>
      </div>
    @endif

@stop
