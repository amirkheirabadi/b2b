@extends('layouts.customer')

@section('header')
  <link href="/dashboard_assets/pages/css/profile.css" rel="stylesheet" type="text/css"/>
@stop

@section('body')
  <!-- BEGIN PAGE HEAD -->
  <div class="page-head">
    <div class="container">
      <!-- BEGIN PAGE TITLE -->
      <div class="page-title">
        <h1>پروفایل کاربری</h1>
      </div>
      <!-- END PAGE TITLE -->
    </div>
  </div>
  <!-- END PAGE HEAD -->
  <!-- BEGIN PAGE CONTENT -->
  <div class="page-content">
    <div class="container">
      		<!-- BEGIN PAGE BREADCRUMB -->
      			<ul class="page-breadcrumb breadcrumb">
      				<li>
      					<a href="#">Home</a><i class="fa fa-circle"></i>
      				</li>
      				<li>
      					<a href="extra_profile_account.html">Pages</a>
      					<i class="fa fa-circle"></i>
      				</li>
      				<li class="active">
      					 User Account
      				</li>
      			</ul>
            <div class="row margin-top-10">
  				<div class="col-md-12">
  					<!-- BEGIN PROFILE SIDEBAR -->
  					<div class="profile-sidebar" style="width: 250px;">
  						<!-- PORTLET MAIN -->
  						<div class="portlet light profile-sidebar-portlet">
  							<!-- SIDEBAR USERPIC -->
  							<div class="profile-userpic">
  								<img src="{{\App\Helper\B2b::gravatar($user->email)}}" class="img-responsive" alt="">
  							</div>
  							<!-- END SIDEBAR USERPIC -->
  							<!-- SIDEBAR USER TITLE -->
  							<div class="profile-usertitle">
  								<div class="profile-usertitle-name">
  									 {{$user->first_name.' '.$user->last_name}}
  								</div>
  							</div>
  							<!-- END SIDEBAR USER TITLE -->
  							<!-- SIDEBAR MENU -->
  							<div class="profile-usermenu">
  								<ul class="nav">
  									<li class="active">
  										<a href="{{URL::to('/dashboard/orders')}}">
  										<i class="icon-settings"></i>
  										  سفارشات</a>
  									</li>
  									<li>
  										<a href="{{URL::to('/dashboard/messages')}}">
  										<i class="icon-check"></i>
  										پیام ها </a>
  									</li>
  									<li>
  										<a href="{{URL::to('/dashboard/transactions')}}">
  										<i class="icon-info"></i>
  										تراکنش ها </a>
  									</li>
  								</ul>
  							</div>
  							<!-- END MENU -->
  						</div>
  						<!-- END PORTLET MAIN -->
  						<!-- PORTLET MAIN -->
  						<div class="portlet light">
  							<!-- STAT -->
  							<div class="row list-separated profile-stat">
  								<div class="col-md-4 col-sm-4 col-xs-6">
  									<div class="uppercase profile-stat-title">
  										 37
  									</div>
  									<div class="uppercase profile-stat-text">
  										سفارش
  									</div>
  								</div>
  								<div class="col-md-4 col-sm-4 col-xs-6">
  									<div class="uppercase profile-stat-title">
  										 51
  									</div>
  									<div class="uppercase profile-stat-text">
  										 پیام
  									</div>
  								</div>
  								<div class="col-md-4 col-sm-4 col-xs-6">
  									<div class="uppercase profile-stat-title">
  										 61
  									</div>
  									<div class="uppercase profile-stat-text">
  										 ماه عوضیت
  									</div>
  								</div>
  							</div>
  							<!-- END STAT -->
  						</div>
  						<!-- END PORTLET MAIN -->
  					</div>
  					<!-- END BEGIN PROFILE SIDEBAR -->
  					<!-- BEGIN PROFILE CONTENT -->
  					<div class="profile-content">
  						<div class="row">
  							<div class="col-md-12">
  								<div class="portlet light">

  									<div class="portlet-body">
                      <form role="form" method="post" action="{{URL::to('/dashboard/profile')}}">
                        <div class="form-group">
                          <label class="control-label">نام</label>
                          <input type="text" name="first_name" class="form-control" value="{{$user->first_name}}">
                        </div>
                        <div class="form-group">
                          <label class="control-label">نام خانوادگی</label>
                          <input type="text" name="last_name" class="form-control" value="{{$user->last_name}}">
                        </div>
                        <div class="form-group">
                          <label class="control-label">آدرس ایمیل</label>
                          <input type="email" name="email" class="form-control" value="{{$user->email}}">
                        </div>
                        <div class="form-group">
                          <label class="control-label">موبایل</label>
                          <input type="text" name="mobile" class="form-control" value="{{$user->mobile}}">
                        </div>
                        <div class="form-group">
                          <label class="control-label">رمز عبور جدید</label>
                          <input type="password" name="password" class="form-control">
                        </div>
                        <div class="form-group">
                          <label class="control-label">تکرار رمز عبور جدید</label>
                          <input type="password" name="password_confirmation" class="form-control">
                        </div>
                        <div class="margiv-top-10">
                          <button type="submit" class="btn green-haze">
                          ذخیره تغییرات </button>
                        </div>
                      </form>
  									</div>
  								</div>
  							</div>
  						</div>
  					</div>
  					<!-- END PROFILE CONTENT -->
  				</div>
  			</div>

      @stop
